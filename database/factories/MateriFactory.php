<?php

namespace Database\Factories;

use App\Models\Materi;
use Illuminate\Database\Eloquent\Factories\Factory;

class MateriFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Materi::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user_id = array_rand(range(1,2));
        $mapel_id = array_rand(range(1,9));
        $kategori_id = array_rand(range(1,7));
        $jenjang = array_rand(range(1,4));
        return [
            'user_id' => array(11,12,13)[$user_id],
            'mapel_id' => array(1,2,3,4,5,6,7,8,9)[$mapel_id],
            'kategori_id' => array(4,5,6,7,8,9,10)[$kategori_id],
            'jenjang' => array('Semua', 'SD', 'SMP', 'SMA', 'SMK')[$jenjang],
            'judul' => $this->faker->text(100),
            'file_video' => 'public/uploads/videos/a7b8b0d5479dae4c46858c8760532b31.mp4',
            'thumbnail' => 'uploads/images/thumbnail/140250d7ec671c87d09dfb4b522f4396.png',
            'deskripsi' => $this->faker->text(100),
            'status' => 'Aktif'
        ];
    }
}
