<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttributeV020ToProposalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposal', function (Blueprint $table) {
            $table->unsignedBigInteger('penilai_id')->after('desa_id')->nullable();
            $table->after('deskripsi', function($table){
                $table->string('attraction', 4)->nullable();
                $table->string('accessbility',4)->nullable();
                $table->string('amenity',4)->nullable();
                $table->string('anciliary',4)->nullable();
                $table->string('promotion',4)->nullable();
                $table->string('final_score',4)->nullable();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposal', function (Blueprint $table) {
            $table->dropColumn('penilai_id');
            $table->dropColumn('attraction');
            $table->dropColumn('accessbility');
            $table->dropColumn('amenity');
            $table->dropColumn('anciliary');
            $table->dropColumn('promotion');
            $table->dropColumn('final_score');
        });
    }
}
