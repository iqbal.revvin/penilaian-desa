<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProposalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposal', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tahun_anggaran_id')->nullable();
            $table->unsignedBigInteger('desa_id')->nullable();
            $table->string('name');
            $table->string('jumlah_pengunjung');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('validasi_pengaju')->nullable();
            $table->string('validasi_penilai')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposal');
    }
}
