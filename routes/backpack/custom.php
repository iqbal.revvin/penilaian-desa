<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

use Illuminate\Support\Facades\Route;


Route::group(['prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge((array) config('backpack.base.web_middleware', 'web'),['web', backpack_middleware()]),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('kategori', 'KategoriCrudController');
    Route::crud('desa', 'DesaCrudController');
    Route::crud('tahunanggaran', 'TahunAnggaranCrudController');
    Route::crud('proposal', 'ProposalCrudController');
    Route::crud('report-proposal', 'ProposalReportController');
    Route::crud('aspect', 'AspectCrudController');
    Route::crud('criteria', 'CriteriaCrudController');
    Route::crud('provision', 'ProvisionCrudController');
    Route::crud('penilai', 'PenilaiCrudController');
});