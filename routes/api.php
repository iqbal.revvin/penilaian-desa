<?php

use App\Http\Controllers\API\Auth\RegisterController;
use App\Http\Controllers\API\MasterData\MasterDataController;
use App\Http\Controllers\API\ProposalService;
use App\Http\Controllers\API\WilayahService;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'auth'],function(){
    Route::post('register', RegisterController::class);
});

Route::group(['prefix' => 'wilayah'], function(){
    Route::get('get-kabupaten', [WilayahService::class, 'getKabupaten'])->name('getKabupatenAPI');
    Route::get('get-kecamatan', [WilayahService::class, 'getKecamatan']);
    Route::get('get-kelurahan', [WilayahService::class, 'getKelurahan']);
});

Route::group(['prefix' => 'data-master'], function(){
    Route::get('category', [MasterDataController::class, 'category']);
});

Route::group(['prefix' => 'proposal'], function(){
    Route::get('get-proposal-questioner/{userId}/{proposalId}', [ProposalService::class, 'GetProposalQuestioner']);
    Route::get('get-proposal-evaluate/{userPenilaiId}/{userDesaId}/{proposalId}/{content?}', [ProposalService::class, 'GetProposalForEvaluate']);
    Route::post('insert-provision', [ProposalService::class, 'InsertProvision']);
    Route::post('insert-image', [ProposalService::class, 'InsertImageQuestioner']);
    Route::post('insert-aspect', [ProposalService::class, 'InsertAspectData']);
    Route::post('submit-proposal/{userId}/{proposalId}', [ProposalService::class, 'SubmitProposal']);
    Route::post('submit-penilai/{userPenilaiId}/{proposalId}', [ProposalService::class, 'ValidasiPenilai']);
});
