<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'proposal';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function openForm($crud = false)
    {
        if($this->validasi_pengaju === 'Divalidasi'){
            return '<a class="btn btn-sm btn-link" target="" href="/admin/proposal-review/'.backpack_user()->id.'/'.$this->id.'" data-toggle="tooltip" title="Riwayat Penilaian"><b><i class="la la-print"></i> Cetak Proposal</b></a>';
        }else{
            return '<a class="btn btn-sm btn-link" target="" href="/admin/proposal-submission/'.backpack_user()->id.'/'.$this->id.'" data-toggle="tooltip" title="Isi Penilaian"><b><i class="la la-files-o"></i> Isi Form Penilaian</b></a>';
        }
    }

    public function openEvaluate($crud = false){
        $desa = Desa::where('id', $this->desa_id)->first();
        if($this->validasi_penilai == 'Divalidasi'){
            return '<a class="btn btn-sm btn-link w-5 text-left" target="" href="/admin/proposal-evaluate/'.backpack_user()->id.'/'.$desa->user_id.'/'.$this->id.'/evaluate-print" data-toggle="tooltip" title="Hasil Penilaian Proposal"><b><i class="la la-print"></i> Cetak Penilaian</b></a>';
        }else{
            return '<a class="btn btn-sm btn-link w-5 text-left" target="" href="/admin/proposal-evaluate/'.backpack_user()->id.'/'.$desa->user_id.'/'.$this->id.'/evaluate" data-toggle="tooltip" title="Penilaian Proposal"><b><i class="la la-check-square-o"></i> Penilaian Proposal</b></a>';
        } 
    }
    public function openReport($crud = false){
        $desa = Desa::where('id', $this->desa_id)->first();
        return '<a class="btn btn-sm btn-link w-5 text-left" target="" href="/admin/proposal-evaluate/'.backpack_user()->id.'/'.$desa->user_id.'/'.$this->id.'/result?type=forprint" data-toggle="tooltip" title="Hasil Penilaian Proposal"><b><i class="la la-print"></i> Cetak Rekap Penilaian</b></a>';
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function tahun_anggaran(){
        return $this->belongsTo(TahunAnggaran::class);
    }
    public function desa(){
        return $this->belongsTo(Desa::class);
    }
    public function penilai(){
        return $this->belongsTo(Penilai::class);
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
