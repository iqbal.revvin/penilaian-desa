<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntryProvision extends Model
{
    use HasFactory;

    protected $table = 'entry_provision';
    protected $guarded = [''];

    public function provision(){
        return $this->belongsTo(Provision::class);
    }

}
