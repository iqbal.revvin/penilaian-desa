<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntryAspect extends Model
{
    use HasFactory;

    protected $table = 'entry_aspect';
    protected $guarded = [''];

}
