<?php

namespace App\Models\Wilayah;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    use HasFactory, CrudTrait;

    protected $table = 'kelurahan';
    protected $guarded = ['id'];

    public function kecamatan(){
        $this->belongsToMany(Kecamatan::class);
    }
}
