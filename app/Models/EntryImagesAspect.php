<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntryImagesAspect extends Model
{
    use HasFactory;

    protected $table = 'entry_images_aspect';
    protected $guarded = [''];

    // public function getImageAttribute($value){
    //     $path = asset('images/submission/'.$value);
    //     return $path;
    // }
}
