<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Desa;
use Illuminate\Foundation\Http\FormRequest;

class DesaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->get('id') ?? request()->route('id');
        if($id){
            $desa = Desa::find($id);
            $emailRule = 'required|email|unique:users,email,'.$desa->user_id;
        }else{
            $emailRule = 'required|email|unique:users,email';
        }
        return [
            'nama' => 'required|min:5|max:255|regex:/^([^0-9]*)$/',
            'no_hp' => 'required|max:14',
            'email' => $emailRule,
            'kepala_desa' => 'required|max:100|string|regex:/^([^0-9]*)$/',
            'link_maps' => 'url||nullable',
            'jumlah_kk' => 'numeric||min:0|gt:0|nullable',
            'jumlah_penduduk' => 'numeric||min:0|gt:0|nullable',
            'jumlah_rt_mbr' => 'numeric|min:0|gt:0|nullable'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
