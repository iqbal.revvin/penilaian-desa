<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Penilai;
use Illuminate\Foundation\Http\FormRequest;

class PenilaiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->get('id') ?? request()->route('id');
        if($id){
            $penilai = Penilai::find($id);
            $emailRule = 'required|email|unique:users,email,'.$penilai->user_id;
        }else{
            $emailRule = 'required|email|unique:users,email';
        }
        return [
            'name' => 'required|min:5|max:255|regex:/^([^0-9]*)$/',
            'email' => $emailRule,
            'no_hp' => 'gt:0|max:99999999999999|numeric|nullable',
            'asal_instansi' => 'max:100',
            'jabatan' => 'max:100'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Nama penilai wajib di isi',
            'name.min' => 'Nama penilai minimal 5 karakter'
        ];
    }
}
