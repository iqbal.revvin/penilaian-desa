<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProposalRequest;
use App\Models\Desa;
use App\Models\Penilai;
use App\Models\Proposal;
use App\Models\TahunAnggaran;
use App\Models\Wilayah\Kecamatan;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;

/**
 * Class ProposalReportController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProposalReportController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Proposal::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/report-proposal');
        CRUD::setEntityNameStrings('Laporan Proposal', 'Laporan Proposal');

        if (!backpack_user()->hasRole('Developer')) {
            $this->crud->denyAccess('create');
            $this->crud->denyAccess('delete');
            $this->crud->denyAccess('update');
        }
    }

    protected function setupListOperation()
    {
        if (backpack_user()->hasRole('Developer') || backpack_user()->hasRole('Admin')) {
            $this->crud->enableExportButtons();
        }
        $this->crud->addButtonFromModelFunction('line', 'open_report', 'openReport', 'beginning');

        if(!backpack_user()->hasRole('Developer')){
            $this->crud->addClause('where', 'validasi_pengaju', 'Divalidasi');
            $this->crud->addClause('where', 'validasi_penilai', 'Divalidasi');
            $this->crud->addClause('where', 'status', 'Diterima Untuk Dinilai');
            if(backpack_user()->hasRole('Penilai')){
                $penilai = Penilai::where('user_id', backpack_user()->id)->first();
                $this->crud->addClause('where', 'penilai_id', $penilai->id);
            }
        }
        $this->crud->column('tahun_anggaran_id')->type('relationship')->attribute('name')->label('Tahun');
        $this->crud->column('name')->label('Nama Wisata');
        $this->crud->column('desa_id')->type('relationship')->attribute('nama');
        $this->crud->column('desa_id')->type('relationship')->attribute('nama');
        $this->crud->addColumn([
            'name'     => 'kecamatan',
            'label'    => 'Kecamatan',
            'type'     => 'closure',
            'function' => function ($entry) {
                return $this->getKecamatan($entry->desa_id);
            }
        ]);
        $this->crud->column('penilai_id')->type('relationship')->attribute('name')->label('Penilai');
        $this->crud->column('attraction');
        $this->crud->column('accessbility');
        $this->crud->column('amenity');
        $this->crud->column('anciliary');
        $this->crud->column('promotion');
        $this->crud->column('final_score')->label('Nilai Akhir');

        $this->crud->modifyColumn('attraction', [
            'wrapper' => [
                'element' => 'span',
                'class' => 'badge badge-secondary'
            ],
        ]);
        $this->crud->modifyColumn('accessbility', [
            'wrapper' => [
                'element' => 'span',
                'class' => 'badge badge-secondary'
            ],
        ]);
        $this->crud->modifyColumn('amenity', [
            'wrapper' => [
                'element' => 'span',
                'class' => 'badge badge-secondary'
            ],
        ]);
        $this->crud->modifyColumn('anciliary', [
            'wrapper' => [
                'element' => 'span',
                'class' => 'badge badge-secondary'
            ],
        ]);
        $this->crud->modifyColumn('promotion', [
            'wrapper' => [
                'element' => 'span',
                'class' => 'badge badge-secondary'
            ],
        ]);
        $this->crud->modifyColumn('final_score', [
            'wrapper' => [
                'element' => 'span',
                'class' => 'badge badge-info'
            ],
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
       
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        // CRUD::setFromDb();
        $this->crud->set('show.setFromDb', false);
        $this->crud->column('desa_id')->type('relationship')->attribute('name')->label('Desa');
        $this->crud->addColumn([
            'name'     => 'kecamatan',
            'label'    => 'Kecamatan',
            'type'     => 'closure',
            'function' => function ($entry) {
                return $this->getKecamatan($entry->desa_id);
            }
        ]);
        $this->crud->column('penilai_id')->type('relationship')->attribute('name')->label('Penilai');
        $this->crud->column('attraction');
        $this->crud->column('accessbility');
        $this->crud->column('amenity');
        $this->crud->column('anciliary');
        $this->crud->column('promotion');
        $this->crud->column('final_score')->label('Nilai Akhir');
    }

    private function getKecamatan($desaId){
        $desa = Desa::where('id', $desaId)->first();
        $kecamatan = Kecamatan::where('id', $desa->kecamatan_id)->first();
        return $kecamatan->name??'-';
    }

}
