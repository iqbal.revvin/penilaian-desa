<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DesaRequest;
use App\Models\Desa;
use App\Models\Wilayah\Kabupaten;
use App\Models\Wilayah\Kecamatan;
use App\Models\Wilayah\Kelurahan;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/**
 * Class DesaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DesaCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Desa::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/desa');
        CRUD::setEntityNameStrings('Desa', 'Desa');

        if(!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')){
            $this->crud->denyAccess('create');
            $this->crud->denyAccess('delete');
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        info(backpack_user()->name.': Access Desa List');
        if (backpack_user()->hasRole('Developer') || backpack_user()->hasRole('Admin')) {
            $this->crud->enableExportButtons();
        }
        CRUD::setFromDb(); // columns
        if(!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')){
            $this->crud->addClause('where', 'user_id', backpack_user()->id);
        }
        $this->crud->modifyColumn('nama',[
            'label' => 'Nama Desa'
        ]);

        $this->crud->column('user_id')->type('relationship')->attribute('email')->label('Akun');
        $this->crud->column('provinsi_id')->type('relationship')->attribute('name');
        $this->crud->column('kabupaten_id')->type('relationship')->attribute('name');
        $this->crud->column('kecamatan_id')->type('relationship')->attribute('name');
        $this->crud->column('kelurahan_id')->type('relationship')->attribute('name');
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        info(backpack_user()->name.': Access Desa Create');
        CRUD::setValidation(DesaRequest::class);

        CRUD::setFromDb(); // fields

        $this->crud->removeField('user_id');
        $this->crud->removeField('latitude');
        $this->crud->removeField('longitude');
        $this->crud->removeField('kelurahan_id');
        if(backpack_user()->hasRole('Desa')){
            $this->crud->removeField('status');
        }

        $this->crud->modifyField('link_maps', [
            'label' => 'Link Maps Desa'
        ]);

        $this->crud->modifyField('no_hp', [
            'type'  => 'number',
            'label' => 'No. Hp / WA'
        ]);
        $this->crud->modifyField('email', [
            'type'  => 'email'
        ]);
        $this->crud->modifyField('kepala_desa', [
            'label' => 'Nama Kepala Desa'
        ]);
        $this->crud->modifyField('jumlah_kk', [
            'label' => 'Jumlah KK'
        ]);
        $this->crud->modifyField('jumlah_rt_mbr', [
            'label' => 'Jumlah RT MBR'
        ]);

        $this->crud->modifyField('provinsi_id', [
            'type' => 'relationship',
            'name' => 'provinsi_id',
            'entity' => 'provinsi', // the relationship name in your Model
            'attribute' => 'name', // attribute on Article that is shown to admin,
        ]);
        $this->crud->modifyField('kabupaten_id', [
            'label' => 'Kabupaten',
            'name' => 'kabupaten_id',
            'type' => 'select2_from_ajax',
            'entity' => 'kabupaten',
            'model' => Kabupaten::class,
            'attribute' => 'name',
            'placeholder' => 'Pilih Kabupaten',
            'minimum_input_length' => 0,
            'dependencies' => ['provinsi_id'],
            'data_source' => route('getKabupatenAPI'),
            'method' => 'GET',
            'include_all_form_fields' => true
        ]);
        $this->crud->modifyField('kecamatan_id', [
            'label' => 'Kecamatan',
            'name' => 'kecamatan_id',
            'type' => 'select2_from_ajax',
            'entity' => 'kecamatan',
            'model' => Kecamatan::class,
            'attribute' => 'name',
            'placeholder' => 'Pilih Kecamatan',
            'minimum_input_length' => 0,
            'dependencies' => ['kabupaten_id'],
            'data_source' => url('api/wilayah/get-kecamatan'),
            'method' => 'GET',
            'include_all_form_fields' => true
        ]);

        // $this->crud->modifyField('kelurahan_id', [
        //     'label' => 'Kelurahan',
        //     'name' => 'kelurahan_id',
        //     'type' => 'select2_from_ajax',
        //     'entity' => 'kelurahan',
        //     'model' => Kelurahan::class,
        //     'attribute' => 'name',
        //     'placeholder' => 'Pilih Kelurahan',
        //     'minimum_input_length' => 0,
        //     'dependencies' => ['kecamatan_id'],
        //     'data_source' => url('api/wilayah/get-kelurahan'),
        //     'method' => 'GET',
        //     'include_all_form_fields' => true
        // ]);

        // $this->crud->modifyField('latitude', [
        //     'label'         => 'Address',
        //     'type'          => 'address_google',
        //     // optional
        //     'store_as_json' => true
        // ]);

        if(!backpack_user()->hasRole('Desa')){
            $this->crud->modifyField('status', [
                'label'       => "Status",
                'type'        => 'select_from_array',
                'options'     => ['Pending' => 'Pending', 'Aktif' => 'Aktif', 'Nonaktif' => 'Nonaktif'],
                'allows_null' => false,
                'default'     => 'Pending',
            ]);
        }
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        info(backpack_user()->name.': Access Desa Update');
        if(!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')){
            $desa = Desa::where('user_id', backpack_user()->id)->first();
            if(Route::current()->parameter('id') == $desa->id){
                $this->setupCreateOperation();
            }else{
                Log::warning(backpack_user()->name.': Force Access Update Desa');
                $this->crud->denyAccess('update');
            }
        }else{
            $this->setupCreateOperation();
        }
    }

    protected function setupShowOperation()
    {
        info(backpack_user()->name.': Access Desa Show');
        if(!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')){
            $desa = Desa::where('user_id', backpack_user()->id)->first();
            if(Route::current()->parameter('id') == $desa->id){
                $this->crud->allowAccess('show');
            }else{
                $this->crud->denyAccess('show');
                Log::warning(backpack_user()->name.': Force Access Show Desa');
            }
        }
    }
    

    
}
