<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProposalRequest;
use App\Models\Desa;
use App\Models\Proposal;
use App\Models\TahunAnggaran;
use App\Models\Wilayah\Kecamatan;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Library\Widget;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/**
 * Class ProposalCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProposalCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \App\Http\Controllers\Admin\Operations\ChangeStatusProposalOperation;
    use \App\Http\Controllers\Admin\Operations\ChangeValidateEvaluatorOperation;
    use \App\Http\Controllers\Admin\Operations\ChangeValidateSubmitorOperation;

    
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Proposal::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/proposal');
        CRUD::setEntityNameStrings('Proposal', 'Proposal');

        if (backpack_user()->hasRole('Penilai')) {
            $this->crud->denyAccess('delete');
            $this->crud->denyAccess('update');
        }

        if (backpack_user()->hasRole('Developer') || backpack_user()->hasRole('Admin') || backpack_user()->hasRole('Penilai')) {
            $this->crud->denyAccess('create');
        }
        if (!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')) {
            $this->crud->denyAccess('changestatusproposal');
            $this->crud->denyAccess('changevalidateevaluator');
            $this->crud->denyAccess('changevalidatesubmitor');
            if (Route::current()->id) {
                $routeId = Route::current()->id;
                $proposal = Proposal::find($routeId);
                if ($proposal->validasi_pengaju == 'Divalidasi') {
                    $this->crud->denyAccess('update');
                }
            }
        }

        if (backpack_user()->hasRole('Desa')) {
            $tahunAnggaran = TahunAnggaran::where('status', 'Aktif')->first();
            if (date('Y-m-d') > $tahunAnggaran->tanggal_akhir_pengajuan) {
                $this->crud->denyAccess('create');
                Widget::add(
                    [
                        'type'         => 'alert',
                        'class'        => 'alert alert-danger',
                        'heading'      => 'Pengajuan Berakhir',
                        'content'      => 'Mohon maaf, pengajuan proposal sudah di tutup, silahkan mengajukan proposal kembali jika pengajuan proposal sudah dibuka!',
                        'close_button' => true, // show close button or not
                        'wrapper' => [
                            'class' => '', // customize the class on the parent element (wrapper)
                            'style' => 'border-radius: 10px;',
                        ]
                    ])->to('before_content');
            }
        }

        // if(!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')){
        //     if ($this->crud->getEntry('id') == 'Divalidasi') {
        //         $this->crud->denyAccess('update');
        //     }
        // }
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns
        info(backpack_user()->name . ': Access Proposal List');
        if (backpack_user()->hasRole('Developer') || backpack_user()->hasRole('Admin')) {
            $this->crud->enableExportButtons();
        }

        $this->crud->removeColumn('deskripsi_singkat');
        $this->crud->column('tahun_anggaran_id')->attribute('name')->label('Tahun');
        if (backpack_user()->hasRole('Penilai') || backpack_user()->hasRole('Admin') || backpack_user()->hasRole('Developer')) {
            $this->crud->addButtonFromModelFunction('line', 'open_evaluate', 'openEvaluate', 'beginning');
        }
        if (backpack_user()->hasRole('Desa')) {
            $this->crud->removeColumn('desa_id');
            $this->crud->addButtonFromModelFunction('line', 'open_form', 'openForm', 'beginning');
        } else {
            $this->crud->column('desa_id')->type('relationship')->attribute('nama');
        }

        $this->crud->addColumn([
            'name'     => 'kecamatan',
            'label'    => 'Kecamatan',
            'type'     => 'closure',
            'function' => function ($entry) {
                return $this->getKecamatan($entry->desa_id);
            }
        ]);
        $this->crud->column('name')->afterColumn('tahun_id');
        $this->crud->column('jumlah_pengunjung')->type('number')->label('Pengunjung');
        $this->crud->column('link_maps');
        $this->crud->column('validasi_pengaju');
        $this->crud->column('validasi_penilai');
        $this->crud->column('status');
        $this->crud->removeColumn('latitude');
        $this->crud->removeColumn('longitude');
        if (backpack_user()->hasRole('Desa')) {
            $this->crud->removeColumn('validasi_penilai');
        }

        $this->crud->modifyColumn('name', [
            'label' => ' Nama Wisata'
        ]);

        $this->crud->modifyColumn('link_maps', [
            'type'         => 'closure',
            'label'        => 'Link Maps DTW', // Table column heading
            'function' => function ($entry) {
                if ($entry->link_maps != null) {
                    return 'Lihat Peta';
                } else {
                    return 'Tidak Tersedia';
                }
            },
            'wrapper' => [
                'element' => 'a',
                'href' => function ($crud, $column, $entry, $related_key) {
                    $public_destination_path = $entry->link_maps;
                    return $public_destination_path;
                },
                'target' => '_blank',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->link_maps != null) {
                        return 'badge badge-info';
                    }
                    return 'badge badge-default';
                },
            ],
        ]);

        if (backpack_user()->hasRole('Penilai')) {
            $this->crud->addClause('where', 'validasi_pengaju', 'Divalidasi');
            $this->crud->addClause('where', 'status', 'Diterima Untuk Dinilai');
        }

        if (!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin') && !backpack_user()->hasRole('Penilai')) {
            $desa = Desa::where('user_id', backpack_user()->id)->first();
            $this->crud->addClause('where', 'desa_id', $desa->id);
        }

        if (backpack_user()->hasRole('Developer') || backpack_user()->hasRole('Admin') || backpack_user()->hasRole('Penilai')) {
            $this->crud->addFilter([
                'name' => 'tahun_anggaran_filter',
                'type' => 'select2',
                'label' => 'Tahun Anggaran'
            ], function () {
                return TahunAnggaran::lazyById()->pluck('name', 'id')->toArray();
            }, function ($value) {
                $this->crud->addClause('where', 'tahun_anggaran_id', $value);
            });

            $this->crud->addFilter([
                'name'  => 'validasi_pengaju',
                'type'  => 'dropdown',
                'label' => 'Validasi Pengaju'
            ], [
                'Pending' => 'Pending',
                'Divalidasi' => 'Divalidasi',
            ], function ($value) { // if the filter is active
                $this->crud->addClause('where', 'validasi_pengaju', $value);
            });

            $this->crud->addFilter([
                'name'  => 'validasi_penilai',
                'type'  => 'dropdown',
                'label' => 'Validasi Penilai'
            ], [
                'Pending' => 'Pending',
                'Divalidasi' => 'Divalidasi',
            ], function ($value) { // if the filter is active
                $this->crud->addClause('where', 'validasi_penilai', $value);
            });

            $this->crud->addFilter([
                'name'  => 'status',
                'type'  => 'dropdown',
                'label' => 'Status'
            ], [
                'Proses' => 'Proses',
                'Diterima Untuk Dinilai' => 'Diterima Untuk Dinilai',
            ], function ($value) { // if the filter is active
                $this->crud->addClause('where', 'status', $value);
            });
        }

        $this->crud->modifyColumn('validasi_pengaju', [
            'type'         => 'text',
            'label'        => 'Validasi Pengaju', // Table column heading
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->validasi_pengaju == 'Pending') {
                        if (!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')) {
                            $this->crud->allowAccess('update');
                        }
                        return 'badge badge-warning';
                    } else if ($entry->validasi_pengaju == 'Divalidasi') {
                        if (!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')) {
                            $this->crud->denyAccess('update');
                        }
                        return 'badge badge-success';
                    }
                    return 'badge badge-secondary';
                },
            ],
        ]);

        $this->crud->modifyColumn('validasi_penilai', [
            'type'         => 'text',
            'label'        => 'Validasi Penilai', // Table column heading
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->validasi_penilai == 'Pending') {
                        return 'badge badge-warning';
                    } else if ($entry->validasi_penilai == 'Divalidasi') {
                        return 'badge badge-success';
                    }
                    return 'badge badge-secondary';
                },
            ],
        ]);


        $this->crud->modifyColumn('status', [
            'type'         => 'text',
            'label'        => 'Status Pengajuan', // Table column heading
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->status == 'Diterima Untuk Dinilai') {
                        return 'badge badge-success';
                    } else if ($entry->status == 'Ditolak') {
                        return 'badge badge-danger';
                    }
                    return 'badge badge-secondary';
                },
            ],
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ProposalRequest::class);

        CRUD::setFromDb(); // fields

        if (!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')) {
            $this->crud->removeField('validasi_penilai');
            $this->crud->removeField('penilai_id');
        }

        $this->crud->removeField('latitude');
        $this->crud->removeField('longitude');

        if (!backpack_user()->hasRole('Developer')) {
            $this->crud->removeField('attraction');
            $this->crud->removeField('accessbility');
            $this->crud->removeField('amenity');
            $this->crud->removeField('anciliary');
            $this->crud->removeField('promotion');
            $this->crud->removeField('final_score');
        }
        if (!backpack_user()->hasRole('Desa')) {
            $this->crud->modifyField('penilai_id', [
                'type' => 'select2',
                'entity' => 'penilai', // the relationship name in your Model
                'attribute' => 'name', // attribute on Article that is shown to admin,
            ]);
        }

        $this->crud->removeField('validasi_pengaju');
        $this->crud->removeField('validasi_penilai');
        $this->crud->removeField('status');

        $this->crud->removeField('tahun_anggaran_id');
        $this->crud->removeField('desa_id');
        $this->crud->removeField('status');

        $this->crud->modifyField('name', [
            'label' => 'Nama Desa Wisata'
        ]);
        $this->crud->modifyField('jumlah_pengunjung', [
            'type' => 'number'
        ]);
        $this->crud->modifyField('deskripsi', [
            'type' => 'textarea',
            'label' => 'Deskripsi Singkat'
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        Log::info(backpack_user()->name . ': Access Update Proposal');
        if (!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin')) {
            $desa = Desa::where('user_id', backpack_user()->id)->first();
            $routeProposalId = Route::current()->parameter('id');
            $proposal = Proposal::where('id', $routeProposalId)->where('desa_id', $desa->id)->first();
            if ($routeProposalId == $proposal->id) {
                $this->setupCreateOperation();
            } else {
                Log::warning(backpack_user()->name . ': Force Update Proposal');
                $this->crud->denyAccess('update');
            }
        } else {
            $this->setupCreateOperation();
        }
        // $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        // CRUD::setFromDb();
        Log::info(backpack_user()->name . ': Access Show Proposal');
        if (!backpack_user()->hasRole('Developer') && !backpack_user()->hasRole('Admin') && !backpack_user()->hasRole('Penilai')) {
            $desa = Desa::where('user_id', backpack_user()->id)->first();
            $routeProposalId = Route::current()->parameter('id');
            $proposal = Proposal::where('id', $routeProposalId)->where('desa_id', $desa->id)->first();
            if ($proposal && $routeProposalId == $proposal->id) {
                Log::warning(backpack_user()->name . ': Force Show Proposal');
                $this->crud->allowAccess('show');
            } else {
                $this->crud->denyAccess('show');
            }
        }

        $this->crud->set('show.setFromDb', false);
        $this->crud->denyAccess('delete');
        $this->crud->column('tahun_anggaran_id')->type('relationship')->attribute('name')->label('Tahun');
        $this->crud->column('name')->afterColumn('tahun_id');
        $this->crud->column('jumlah_pengunjung')->type('number')->label('Pengunjung');
        $this->crud->addColumn('link_maps');
        $this->crud->column('validasi_pengaju');
        $this->crud->column('status');
        $this->crud->column('deskripsi');
        $this->crud->removeColumn('validasi_penilai');
        $this->crud->modifyColumn('link_maps', [
            'type'         => 'closure',
            'label'        => 'Link Maps DTW', // Table column heading
            'function' => function ($entry) {
                if ($entry->link_maps != null) {
                    return 'LIhat Peta';
                } else {
                    return 'Tidak Tersedia';
                }
            },
            'wrapper' => [
                'element' => 'a',
                'href' => function ($crud, $column, $entry, $related_key) {
                    $public_destination_path = $entry->link_maps;
                    return $public_destination_path;
                },
                'target' => '_blank',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->link_maps != null) {
                        return 'badge badge-info';
                    }
                    return 'badge badge-default';
                },
            ],
        ]);
    }

    private function getKecamatan($desaId)
    {
        $desa = Desa::where('id', $desaId)->first();
        $kecamatan = Kecamatan::where('id', $desa->kecamatan_id)->first();
        return $kecamatan->name ?? '-';
    }
}
