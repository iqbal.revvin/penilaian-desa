<?php

namespace App\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;

trait ChangeStatusOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupChangeStatusRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/{id}/changestatus', [
            'as'        => $routeName.'.changestatus',
            'uses'      => $controller.'@changestatus',
            'operation' => 'changestatus',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupChangeStatusDefaults()
    {
        $this->crud->allowAccess('changestatus');

        $this->crud->operation('changestatus', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            // $this->crud->addButton('top', 'changestatus', 'view', 'crud::buttons.changestatus');
            // $this->crud->addButton('line', 'changestatus', 'view', 'crud::buttons.changestatus');
            $this->crud->addButtonFromView('line', 'changestatus', 'changestatus', 'beginning');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function changestatus($id)
    {
        // $this->crud->hasAccessOrFail('changestatus');

        // prepare the fields you need to show
        $changeStatusEntry = $this->crud->model->find($id);
        $changeStatusEntry->status =  $changeStatusEntry->status=='Aktif'?'NonAktif':'Aktif';
        $changeStatusEntry->save();

        return (string) $changeStatusEntry->push();
    }
}
