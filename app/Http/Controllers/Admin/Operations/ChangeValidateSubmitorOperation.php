<?php

namespace App\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;

trait ChangeValidateSubmitorOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupChangeValidateSubmitorRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/{id}/changevalidatesubmitor', [
            'as'        => $routeName.'.changevalidatesubmitor',
            'uses'      => $controller.'@changevalidatesubmitor',
            'operation' => 'changevalidatesubmitor',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupChangeValidateSubmitorDefaults()
    {
        $this->crud->allowAccess('changevalidatesubmitor');

        $this->crud->operation('changevalidatesubmitor', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            // $this->crud->addButton('top', 'changevalidatesubmitor', 'view', 'crud::buttons.changevalidatesubmitor');
            // $this->crud->addButton('line', 'changevalidatesubmitor', 'view', 'crud::buttons.changevalidatesubmitor');
            $this->crud->addButtonFromView('line', 'changevalidatesubmitor', 'changevalidatesubmitor', 'beginning');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function changevalidatesubmitor($id)
    {
        $changeValidateSubmitor = $this->crud->model->find($id);
        $changeValidateSubmitor->validasi_pengaju =  $changeValidateSubmitor->validasi_pengaju == 'Pending'?'Divalidasi':'Pending';
        $changeValidateSubmitor->save();

        return (string) $changeValidateSubmitor->push();
    }
}
