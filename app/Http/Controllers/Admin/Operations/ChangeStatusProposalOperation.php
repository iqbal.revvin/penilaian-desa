<?php

namespace App\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;

trait ChangeStatusProposalOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupChangeStatusProposalRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/{id}/changestatusproposal', [
            'as'        => $routeName.'.changestatusproposal',
            'uses'      => $controller.'@changestatusproposal',
            'operation' => 'changestatusproposal',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupChangeStatusProposalDefaults()
    {
        $this->crud->allowAccess('changestatusproposal');

        $this->crud->operation('changestatusproposal', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            // $this->crud->addButton('top', 'changestatusproposal', 'view', 'crud::buttons.changestatusproposal');
            // $this->crud->addButton('line', 'changestatusproposal', 'view', 'crud::buttons.changestatusproposal');
            $this->crud->addButtonFromView('line', 'changestatusproposal', 'changestatusproposal', 'beginning');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function changestatusproposal($id)
    {
        $changeStatusEntry = $this->crud->model->find($id);
        $changeStatusEntry->status =  $changeStatusEntry->status=='Proses'?'Diterima Untuk Dinilai':'Proses';
        $changeStatusEntry->save();

        return (string) $changeStatusEntry->push();
    }
}
