<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TahunAnggaranRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TahunAnggaranCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TahunAnggaranCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\TahunAnggaran::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/tahunanggaran');
        CRUD::setEntityNameStrings('Tahun Anggaran', 'Tahun Anggaran');
        if(!backpack_user()->hasRole('Developer')){
            $this->crud->denyAccess('create');
            $this->crud->denyAccess('delete');
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns
        $this->crud->modifyColumn('name', [
            'label' => 'Tahun Anggaran'
        ]);
        $this->crud->modifyColumn('status', [
            'type'         => 'text',
            'label'        => 'Status', // Table column heading
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->status == 'Aktif') {
                        return 'badge badge-success';
                    }else if($entry->status == 'NonAktif'){
                        return 'badge badge-danger';
                    }
                    return 'badge badge-warning';
                },
            ],
        ]);

    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(TahunAnggaranRequest::class);

        CRUD::setFromDb(); // fields
        $this->crud->modifyField('name', [
            'type' => 'number',
            'label' => 'Nama Tahun'
        ]);

        $this->crud->modifyField('status', [
            'label'       => "Status",
            'type'        => 'select_from_array',
            'options'     => ['Pending' => 'Pending', 'Aktif' => 'Aktif', 'Nonaktif' => 'Nonaktif'],
            'allows_null' => false,
            'default'     => 'Pending',
        ]);

    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
