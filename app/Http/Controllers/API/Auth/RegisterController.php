<?php

namespace App\Http\Controllers\API\Auth;

use App\Helpers\GlobalHelper;
use App\Http\Controllers\Controller;
use App\Models\Tenpen;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'sekolah_id' => ['required'],
            'nama_lengkap' => ['required', 'min:3'],
            'jabatan' => ['required', 'min:3'],
            'jenis_kelamin' => ['required', 'min:3'],
            'email' => ['email', 'required', 'unique:users,email'],
            'password' => ['required', 'min:6'],
            'role' => ['required', 'string']
        ]);

        if($validator->passes()){
            $user = new User;
            $user->name = $request->nama_lengkap;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();

            if($user->save()){
                $user->assignRole($request->role);
                $tenpen = new Tenpen;
                $tenpen->user_id = $user->id;
                $tenpen->sekolah_id = $request->sekolah_id;
                $tenpen->nama_lengkap = $request->nama_lengkap;
                $tenpen->jabatan = $request->jabatan;
                $tenpen->jenis_kelamin = $request->jenis_kelamin;
                $tenpen->email = $request->email;
                $tenpen->save();
            }

            if($tenpen->save()){
                
                return GlobalHelper::createResponse(true, 'Terimakasih, Pendaftaran berhasil', $user);
            }else{
                return GLobalHelper::createResponse(false, 'Mohon Maaf, Terjadi kesalahan', $user);
            }
            
        }else{
            return GlobalHelper::createResponse(false, 'Mohon maaf, pendaftaran gagal, silahkan periksa kembali inputan', $validator->errors()->all());
        }
    }
}
