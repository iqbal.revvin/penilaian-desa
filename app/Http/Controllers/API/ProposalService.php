<?php

namespace App\Http\Controllers\API;

use App\Helpers\GlobalHelper;
use App\Http\Controllers\Controller;
use App\Models\Aspect;
use App\Models\Criteria;
use App\Models\Desa;
use App\Models\EntryAspect;
use App\Models\EntryImagesAspect;
use App\Models\EntryProvision;
use App\Models\Penilai;
use App\Models\Proposal;
use App\Models\Provision;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ProposalService extends Controller
{
    public function index()
    {
        return 'hello!';
    }

    public function validasiPenilai(Request $request)
    {
        // dd($request);
        if ($request->proposalId) {
            $proposal = Proposal::find($request->proposalId);
            $penilai = Penilai::where('user_id', $request->penilai_id)->first();
            $proposal->update([
                'validasi_penilai' => 'Divalidasi',
                'penilai_id' => $penilai->id,
                'attraction' => $request->attraction,
                'accessbility' => $request->accessbility,
                'amenity' => $request->anciliary,
                'anciliary' => $request->anciliary,
                'promotion' => $request->promotion,
                'final_score' => $request->final_score
            ]);
        } else {
            return GlobalHelper::createResponse(false, 'Gagal memvalidasi nilai');
        }
    }

    public function SubmitProposal(Request $request)
    {
        $desa = Desa::where('user_id', $request->userId)->first();
        $proposal = Proposal::where('id', $request->proposalId)->where('desa_id', $desa->id);
        if ($proposal) {
            $proposal->update(['validasi_pengaju' => 'Divalidasi']);
            return GlobalHelper::createResponse(true, 'Proposal berhasil di validasi');
        }
        return GlobalHelper::createResponse(false, 'Gagal memvalidasi proposal');
    }

    public function GetProposalForEvaluate(Request $request)
    {
        $user = User::find($request->userPenilaiId);
        $desa = Desa::where('user_id', $request->userDesaId)->first();
        $proposal = Proposal::where('id', $request->proposalId)->where('desa_id', $desa->id);
        if (!$user->hasRole('Penilai') && !$user->hasRole('Developer') && !$user->hasRole('Admin')) {
            return GlobalHelper::createResponse(false, 'proposal-for-evaluator');
        } else {
            if ($proposal->first()->validasi_pengaju == 'Pending') {
                return GlobalHelper::createResponse(false, 'proposal-not-ready');
            }
            if (
                $proposal->first()->validasi_penilai == 'Divalidasi'
                && !$user->hasRole('Developer')
                && $request->content != 'result'
                && $request->content != 'evaluate-print'
            ) {
                return GlobalHelper::createResponse(false, 'proposal-is-evaluate');
            }
            return $this->dataProposalForEvaluate($request->userPenilaiId, $request->userDesaId, $request->proposalId);
        }
    }

    public function GetProposalQuestioner(Request $request)
    {
        $desa = Desa::where('user_id', $request->userId)->first();
        $proposal = Proposal::where('id', $request->proposalId)->where('desa_id', $desa->id);
        $no_hp = GlobalHelper::noHp($desa->no_hp);
        $proposal_submission = [
            'nama_desa' => $desa->nama ?? '-',
            'nama_desa_wisata' => $proposal->first()->name ?? '-',
            'nama_kepala_desa' => $desa->kepala_desa ?? '-',
            'email' => $desa->email ?? '-',
            'no_hp' => $no_hp ?? '-',
            'provinsi' => $proposal->first()->desa->provinsi->name ?? '-',
            'kabupaten' => $proposal->first()->desa->kabupaten->name ?? '-',
            'kecamatan' => $proposal->first()->desa->kecamatan->name ?? '-',
            'desa' => $proposal->first()->desa->kelurahan->name ?? '-',
            'jumlah_pengunjung' => $proposal->first()->jumlah_pengunjung ?? '-'
        ];
        if ($desa) {
            $checkDataProposal = $proposal->exists();
        } else {
            return GlobalHelper::createResponse(false, 'Failed get data Questioner');
        }
        if (!$checkDataProposal) {
            return GlobalHelper::createResponse(false, 'Failed get data Questioner');
        }
        if ($request->input('type') == 'submission') {
            if ($proposal->first()->validasi_pengaju != 'Pending') {
                return GlobalHelper::createResponse(false, 'proposal-submited');
            }
        }
        $criteria = Criteria::with('aspect')->get();

        $data_questioner_proposal = $criteria->map(function ($value, $idx) use ($request) {
            return [
                'id' => $value->id,
                'name' => $value->name,
                'aspect' => $value->aspect->map(function ($valAspect, $idxAspect) use ($request) {
                    $user_aspect = EntryAspect::where('user_id', $request->userId)->where('proposal_id', $request->proposalId)->where('aspect_id', $valAspect->id);
                    $user_description = $user_aspect->pluck('description')->first();
                    $user_video_1 = $user_aspect->pluck('video_url_1')->first();
                    $user_video_2 = $user_aspect->pluck('video_url_2')->first();
                    return [
                        'id' => $valAspect->id,
                        'name' => $valAspect->name,
                        'description_entry' => $user_description ?? '',
                        'images' => [
                            ['id' => 1, 'name' => 'image_1', 'url' => $this->getImageAspectItem($request->userId, $request->proposalId, $valAspect->id, 'image_1')],
                            ['id' => 2, 'name' => 'image_2', 'url' => $this->getImageAspectItem($request->userId, $request->proposalId, $valAspect->id, 'image_2')],
                            ['id' => 3, 'name' => 'image_3', 'url' => $this->getImageAspectItem($request->userId, $request->proposalId, $valAspect->id, 'image_3')],
                            ['id' => 4, 'name' => 'image_4', 'url' => $this->getImageAspectItem($request->userId, $request->proposalId, $valAspect->id, 'image_4')],
                            ['id' => 5, 'name' => 'image_5', 'url' => $this->getImageAspectItem($request->userId, $request->proposalId, $valAspect->id, 'image_5')],
                            ['id' => 6, 'name' => 'image_6', 'url' => $this->getImageAspectItem($request->userId, $request->proposalId, $valAspect->id, 'image_6')],
                        ],
                        'video_url_1' => $user_video_1 ?? '',
                        'video_url_2' => $user_video_2 ?? '',
                        'provision' => $valAspect->provision->map(function ($valProvision, $idxProvisino) use ($request) {
                            $checkProvisionSelected = EntryProvision::where('proposal_id', $request->proposalId)->where('provision_id', $valProvision->id)->exists();
                            return [
                                'id' => $valProvision->id,
                                'name' => $valProvision->name,
                                'selected' => $checkProvisionSelected
                            ];
                        })
                    ];
                })
            ];
        });
        $data = [
            'submission_profile' => $proposal_submission,
            'questioner' => $data_questioner_proposal
        ];
        if ($criteria) {
            return GlobalHelper::createResponse(true, 'Data Questioner has successfuly retrieve', $data);
        } else {
            return GlobalHelper::createResponse(false, 'Failed get data Questioner');
        }
    }

    public function InsertAspectData(Request $request)
    {
        $aspects = $request->aspects;
        if ($aspects) {
            foreach ($request->aspects as $item) {
                $aspect = EntryAspect::updateOrCreate(
                    ['user_id' => $item['user_id'], 'proposal_id' => $item['proposal_id'], 'aspect_id' => $item['aspect_id']],
                    ['description' => $item['description'], 'video_url_1' => $item['video_url_1'], 'video_url_2' => $item['video_url_2']]
                );
            }
            return GlobalHelper::createResponse(true, 'Aspect data sucessfuly saved', $aspect);
        }
        return GlobalHelper::createResponse(false, 'Please fill data');
    }

    public function InsertProvision(Request $request)
    {
        $requestProvision = $request->provisions;
        $dataInsertTemp = [];
        foreach ($requestProvision as $items) {
            $checkData = EntryProvision::where('user_id', $items['user_id'])
                ->where('proposal_id', $items['proposal_id'])
                ->where('aspect_id', $items['aspect_id']);
            if ($checkData->exists()) {
                $checkData->update(['provision_id' => $items['provision_id']]);
            }
            if (!$checkData->where('provision_id', $items['provision_id'])->exists()) {
                array_push($dataInsertTemp, $items);
            }
        }
        $entry = EntryProvision::insert($dataInsertTemp);
        if ($entry) {
            return GlobalHelper::createResponse(true, 'Success Save Provision Data', $entry);
        } else {
            return GlobalHelper::createResponse(false, 'Failed insert provision');
        }
    }


    public function InsertImageQuestioner(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if ($validator->fails()) {
            $imageCheck = EntryImagesAspect::where('user_id', $request->user_id)->where('proposal_id', $request->proposal_id)->where('aspect_id', $request->aspect_id)->where('name', $request->name);
            if ($imageCheck->exists()) {
                $imagePath = public_path() . '/images/submission';
                $getImagePath = $imagePath . '/' . $imageCheck->first()->image;
                if ($imageCheck->first()->image != null) {
                    unlink($getImagePath);
                }
                $imageCheck->update(['image' => null]);
                return GlobalHelper::createResponse(true, 'Image Deleted');
            } else {
                return GlobalHelper::createResponse(false, 'Failed Upload Image', $validator->messages()->first());
            }
        }

        $image = new Image;
        $getImage = $request->image;
        $imageName = time() . '.' . $getImage->extension();
        $imagePath = public_path() . '/images/submission';

        $image->path = $imagePath;
        $image->image = $imageName;

        $dataInsert = [
            'user_id' => $request->user_id,
            'proposal_id' => $request->proposal_id,
            'aspect_id' => $request->aspect_id,
            'name' => $request->name,
            'image' => $imageName
        ];

        $validateImage = EntryImagesAspect::where('user_id', $request->user_id)->where('proposal_id', $request->proposal_id)->where('aspect_id', $request->aspect_id);
        if ($validateImage->get()->count() < 6) {
            $checkImageName = $validateImage->where('name', $request->name)->exists();
            if ($checkImageName) {
                $getImagePath = $imagePath . '/' . $validateImage->first()->image;
                if ($validateImage->where('name', $request->name)->first()->image != null) unlink($getImagePath);
                $getImage->move($imagePath, $imageName);
                $update = $validateImage->where('name', $request->name)->update($dataInsert);
                if ($update) return GlobalHelper::createResponse(true, 'Images has been update', $update);
            } else {
                $getImage->move($imagePath, $imageName);
                $insert = EntryImagesAspect::create($dataInsert);
                if ($insert) return GlobalHelper::createResponse(true, 'Images has been saved', $insert);
            }
            return GlobalHelper::createResponse(false, 'Failed upload image');
        } else {
            return GlobalHelper::createResponse(false, 'Upload Gambar Dibatasi hanya 6 gambar');
        }
    }


    private function getImageAspectItem($userId, $proposalId, $aspectId, $name)
    {
        $images_data = EntryImagesAspect::where('user_id', $userId)->where('proposal_id', $proposalId)->where('aspect_id', $aspectId)->where('name', $name)->pluck('image')->first();
        if ($images_data) {
            return asset('images/submission/' . $images_data);
        } else {
            return null;
        }
    }

    private function dataProposalForEvaluate($userPenilaiId, $userDesaId, $proposalId)
    {
        $desa = Desa::where('user_id', $userDesaId)->first();
        $proposal = Proposal::where('id', $proposalId)->where('desa_id', $desa->id);
        $no_hp = GlobalHelper::noHp($desa->no_hp);
        $proposal_submission = [
            'nama_desa' => $desa->nama,
            'nama_desa_wisata' => $proposal->first()->name,
            'nama_kepala_desa' => $desa->kepala_desa ?? '-',
            'email' => $desa->email ?? '-',
            'no_hp' => $no_hp ?? '-',
            'provinsi' => $proposal->first()->desa->provinsi->name ?? '-',
            'kabupaten' => $proposal->first()->desa->kabupaten->name ?? '-',
            'kecamatan' => $proposal->first()->desa->kecamatan->name ?? '-',
            'desa' => $proposal->first()->desa->kelurahan->name ?? '-',
            'jumlah_pengunjung' => $proposal->first()->jumlah_pengunjung
        ];
        $penilai = Penilai::where('user_id', $userPenilaiId)->first();
        $penilai_profile = [
            'nama_penilai' => $penilai->name ?? 'Administrator System',
            'asal_instansi' => $penilai->asal_instansi ?? '-',
            'jabatan' => $penilai->jabatan ?? '-',
            'penilai_validate' => $proposal->first()->validasi_penilai
        ];

        $criteria = Criteria::with('aspect')->get();
        $data_questioner_proposal = $criteria->map(function ($value, $idx) use ($userDesaId, $proposalId) {
            return [
                'id' => $value->id,
                'name' => $value->name,
                'weight' => $value->weight,
                'aspect' => $value->aspect->map(function ($valAspect, $idxAspect) use ($userDesaId, $proposalId) {
                    $user_aspect = EntryAspect::where('user_id', $userDesaId)->where('proposal_id', $proposalId)->where('aspect_id', $valAspect->id);
                    $user_description = $user_aspect->pluck('description')->first();
                    $user_video_1 = $user_aspect->pluck('video_url_1')->first();
                    $user_video_2 = $user_aspect->pluck('video_url_2')->first();
                    return [
                        'id' => $valAspect->id,
                        'name' => $valAspect->name,
                        'weight' => $valAspect->weight,
                        'description_entry' => $user_description ?? '',
                        'images' => [
                            ['id' => 1, 'name' => 'image_1', 'url' => $this->getImageAspectItem($userDesaId, $proposalId, $valAspect->id, 'image_1')],
                            ['id' => 2, 'name' => 'image_2', 'url' => $this->getImageAspectItem($userDesaId, $proposalId, $valAspect->id, 'image_2')],
                            ['id' => 3, 'name' => 'image_3', 'url' => $this->getImageAspectItem($userDesaId, $proposalId, $valAspect->id, 'image_3')],
                            ['id' => 4, 'name' => 'image_4', 'url' => $this->getImageAspectItem($userDesaId, $proposalId, $valAspect->id, 'image_4')],
                            ['id' => 5, 'name' => 'image_5', 'url' => $this->getImageAspectItem($userDesaId, $proposalId, $valAspect->id, 'image_5')],
                            ['id' => 6, 'name' => 'image_6', 'url' => $this->getImageAspectItem($userDesaId, $proposalId, $valAspect->id, 'image_6')],
                        ],
                        'video_url_1' => $user_video_1 ?? '',
                        'video_url_2' => $user_video_2 ?? '',
                        'provision' => $valAspect->provision->map(function ($valProvision, $idxProvisino) use ($proposalId) {
                            $checkProvisionSelected = EntryProvision::where('proposal_id', $proposalId)->where('provision_id', $valProvision->id)->exists();
                            return [
                                'id' => $valProvision->id,
                                'name' => $valProvision->name,
                                'weight' => $valProvision->value,
                                'selected' => $checkProvisionSelected
                            ];
                        })
                    ];
                })
            ];
        });

        foreach ($criteria as $criteria) {
            $forAspect = $criteria->aspect->map(function ($valAspect, $idxAspect) use ($proposalId) {
                $bobot_aspek = $valAspect->weight / 100;
                $provision = $valAspect->provision->map(function ($valProvision, $provisionIdx) use ($valAspect, $proposalId, $bobot_aspek) {
                    $provisionSelected = EntryProvision::where('proposal_id', $proposalId)->where('aspect_id', $valAspect->id)->where('provision_id', $valProvision->id)->exists();
                    if ($provisionSelected) {
                        return $valProvision->value * $bobot_aspek;
                    }
                });
                $dataValueProvision = $provision->filter(function ($value, $idx) {
                    return $value != null;
                });
                return $dataValueProvision->first();
            });
            $bobot_criteria = $criteria->weight / 100;
            $finalResultData[] = [
                'id' => $criteria->id,
                'name' => $criteria->name,
                'aspect' => round($forAspect->sum() * $bobot_criteria, 1)
            ];
        }

        // dd($finalResultData);

        $data = [
            'penilai_profile' => $penilai_profile,
            'submission_profile' => $proposal_submission,
            'questioner' => $data_questioner_proposal,
            'final_result' => $finalResultData
        ];

        if ($criteria) {
            return GlobalHelper::createResponse(true, 'Data Questioner has successfuly retrieve', $data);
        } else {
            return GlobalHelper::createResponse(false, 'Failed get data Questioner');
        }
    }
}
