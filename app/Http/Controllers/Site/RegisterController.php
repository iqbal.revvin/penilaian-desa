<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index(){
        $title = 'Register';

        $compact = compact('title');

        return view('register', $compact);
    }
}
