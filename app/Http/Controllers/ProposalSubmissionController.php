<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProposalSubmissionController extends Controller
{
    public function index(){
        return 'hello';
    }

    public function ProposalSubmission(){
        return view('proposal');
    }

    public function ProposalReview(){
        return view('proposal_review');
    }

    public function ProposalEvaluate(){
        return view('proposal_evaluate');
    }
}