<?php

namespace App\Observers;

use App\Models\Desa;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class DesaObserver
{
    /**
     * Handle the Desa "created" event.
     *
     * @param  \App\Models\Desa  $desa
     * @return void
     */
    public function created(Desa $desa)
    {
        $user = new User;
        $user->name = $desa->nama;
        $user->email = $desa->email;
        $user->password = Hash::make('pendes2021');
        $user->save();

        if($user->save()){
            $user->assignRole('Desa');
            $desa->user_id = $user->id;
            $desa->save();
        }
    }

    /**
     * Handle the Desa "updated" event.
     *
     * @param  \App\Models\Desa  $desa
     * @return void
     */
    public function updated(Desa $desa)
    {
        //
    }

    /**
     * Handle the Desa "deleted" event.
     *
     * @param  \App\Models\Desa  $desa
     * @return void
     */
    public function deleted(Desa $desa)
    {
        //
    }

     /**
     * Handle the Tenpen "deleting" event.
     *
     * @param  \App\Models\Desa  $Desa
     * @return void
     */
    public function deleting(Desa $desa)
    {
        $user = User::query();
        $user_exists = $user->where('id', $desa->user_id)->exists();
        if($user_exists){
            $user->where('id', $desa->user_id)->delete();
        }
        // dd($tenpen->user_id);
    }

    /**
     * Handle the Desa "restored" event.
     *
     * @param  \App\Models\Desa  $desa
     * @return void
     */
    public function restored(Desa $desa)
    {
        //
    }

    /**
     * Handle the Desa "force deleted" event.
     *
     * @param  \App\Models\Desa  $desa
     * @return void
     */
    public function forceDeleted(Desa $desa)
    {
        //
    }
}
