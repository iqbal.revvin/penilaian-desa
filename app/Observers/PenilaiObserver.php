<?php

namespace App\Observers;

use App\Models\Penilai;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class PenilaiObserver
{
    /**
     * Handle the Penilai "created" event.
     *
     * @param  \App\Models\Penilai  $penilai
     * @return void
     */
    public function created(Penilai $penilai)
    {
        $user = new User;
        $user->name = $penilai->name;
        $user->email = $penilai->email;
        $user->password = Hash::make($penilai->email);
        $user->save();

        if($user->save()){
            $user->assignRole('Penilai');
            $penilai->user_id = $user->id;
            $penilai->save();
        }
    }

    /**
     * Handle the Penilai "updated" event.
     *
     * @param  \App\Models\Penilai  $penilai
     * @return void
     */
    public function updated(Penilai $penilai)
    {
        //
    }

    /**
     * Handle the Penilai "deleted" event.
     *
     * @param  \App\Models\Penilai  $penilai
     * @return void
     */
    public function deleted(Penilai $penilai)
    {
        //
    }

    /**
     * Handle the Tenpen "deleting" event.
     *
     * @param  \App\Models\Desa  $Desa
     * @return void
     */
    public function deleting(Penilai $penilai)
    {
        $user = User::query();
        $user_exists = $user->where('id', $penilai->user_id)->exists();
        if($user_exists){
            $user->where('id', $penilai->user_id)->delete();
        }
        // dd($tenpen->user_id);
    }

    /**
     * Handle the Penilai "restored" event.
     *
     * @param  \App\Models\Penilai  $penilai
     * @return void
     */
    public function restored(Penilai $penilai)
    {
        //
    }

    /**
     * Handle the Penilai "force deleted" event.
     *
     * @param  \App\Models\Penilai  $penilai
     * @return void
     */
    public function forceDeleted(Penilai $penilai)
    {
        //
    }
}
