<?php

namespace App\Observers;

use App\Models\Desa;
use App\Models\Proposal;
use App\Models\TahunAnggaran;

class ProposalObserver
{
    /**
     * Handle the Proposal "created" event.
     *
     * @param  \App\Models\Proposal  $proposal
     * @return void
     */
    public function created(Proposal $proposal)
    {
        $tahun_anggaran = TahunAnggaran::where('status', 'Aktif')->first();
        $desa = Desa::where('user_id', backpack_user()->id)->first();
        
        $proposal->tahun_anggaran_id = $tahun_anggaran->id;
        $proposal->desa_id = $desa->id;
        $proposal->save();
    }

    /**
     * Handle the Proposal "updated" event.
     *
     * @param  \App\Models\Proposal  $proposal
     * @return void
     */
    public function updated(Proposal $proposal)
    {
        //
    }

    /**
     * Handle the Proposal "deleted" event.
     *
     * @param  \App\Models\Proposal  $proposal
     * @return void
     */
    public function deleted(Proposal $proposal)
    {
        //
    }

    /**
     * Handle the Proposal "restored" event.
     *
     * @param  \App\Models\Proposal  $proposal
     * @return void
     */
    public function restored(Proposal $proposal)
    {
        //
    }

    /**
     * Handle the Proposal "force deleted" event.
     *
     * @param  \App\Models\Proposal  $proposal
     * @return void
     */
    public function forceDeleted(Proposal $proposal)
    {
        //
    }
}
