<?php

namespace App\Providers;

use App\Models\Desa;
use App\Models\Penilai;
use App\Models\Proposal;
use App\Models\Role;
use App\Models\User;
use App\Observers\DesaObserver;
use App\Observers\PenilaiObserver;
use App\Observers\ProposalObserver;
use App\Observers\RoleObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Role::observe(RoleObserver::class);
        User::observe(UserObserver::class);
        Desa::observe(DesaObserver::class);
        Proposal::observe(ProposalObserver::class);
        Penilai::observe(PenilaiObserver::class);
    }
}
