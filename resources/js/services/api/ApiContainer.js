import axios from 'axios'

//BASE-URL
const BASE_URL = process.env.MIX_API_URL

//API-KEY
const API_KEY = process.env.MIX_API_KEY

//ENDPOINT
export const GET_QUESTIONER_PROPOSAL = 'proposal/get-proposal-questioner'
export const GET_PROPOSAL_EVALUATE = 'proposal/get-proposal-evaluate'
export const INSERT_PROVISION = 'proposal/insert-provision'
export const INSERT_ASPECT = 'proposal/insert-aspect'
export const SUBMIT_PROPOSAL = 'proposal/submit-proposal'
export const INSERT_IMAGE = 'proposal/insert-image'
export const VALIDASI_PENILAI = 'proposal/submit-penilai'

const API = axios.create({
    headers: { 
        'Access-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        'x-api-key': API_KEY
      },
    baseURL: BASE_URL
})

export default API