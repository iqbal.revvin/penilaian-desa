import { applyMiddleware, compose, createStore } from "redux";
import {persistStore, persistReducer} from 'redux-persist'
import reducer from "./reducers";
import storage from 'redux-persist/lib/storage'

const persistConfig = {
    key: 'root',
    storage: storage,
    blacklist:[''],
    whitelist:['']
}

const persistedReducer = persistReducer(persistConfig, reducer)

// let composeEnhancers = false;
// if (softwareStage === 'dev') {
//   composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
// }
const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const Store = createStore(persistedReducer, composeEnhancers ? composeEnhancers(applyMiddleware()) : undefined)
const Persistor = persistStore(Store)

export {Store, Persistor}