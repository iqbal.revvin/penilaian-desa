export const UPDATE_FINAL_RESULT = 'UPDATE-FINAL-RESULT'
export const UPDATE_PENILAI_PROFILE = 'UPDATE-PENILAI-PROFILE'
export const UPDATE_SUBMISSION_PROFILE = 'UPDATE-SUBMISSION-PROFILE'
export const UPDATE_MASTER_DATA_CRITERIA = 'UPDATE-MASTER-DATA-CRITERIA'
export const UPDATE_PROVISION_DATA = 'UPDATE-PROVISION-DATA'
export const UPDATE_ASPECT_DATA = 'UPDATE-ASPECT-DATA'
export const SELECT_CRITERIA = 'SELECT-CRITERIA'
export const SELECT_PROVISION = 'SELECT-PROVISION'
export const ENTRY_DESCRIPTION = 'ENTRY_DESCRIPTION'
export const CHANGE_IMAGE = 'CHANGE_IMAGE'
export const CHANGE_VIDEO_URL = 'CHANGE_VIDEO_URL'
export const INSERT_PROVISION_DATA = 'INSERT_PROVISION_DATA'
export const SET_VALIDATE_ALLOW = 'SET-VALIDATE-ALLOW'
export const SET_VALIDATE_DENY = 'SET-VALIDATE-DENY'

const initialState = {
    validate:false,
    penilai_profile: {
        nama_penilai: '-',
        asal_instansi: '-',
        jabatan: '-'
    },
    provision_data : [],
    aspect_data_value: [],
    submission_profile: {
        nama_desa_wisata: '',
        kecamatan: '',
        desa: ''
    },
    criteria: [],
    final_result:[],
    criteria_sample: [
        {
            id:1, name:'Daya Tarik / Attraction', selected:false, aspect:[
                {id:1, name:'Pemandangan', description_entry:'Hello', video_url_1:'https://youtu.be/Cn0zoLM_oT8', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/1/425/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/2/425/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/3/425/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/4/425/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/5/425/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/6/425/300'},
                    ],
                    provision:[
                        {id: 1, name:'Tidak memiliki pemandangan alam perdesaan yang sangat menarik', weight:0, selected:false},
                        {id: 2, name: 'Memiliki pemandangan alam perdesaan yang kurang menarik', weight: 50, selected:false},
                        {id: 3, name: 'Memiliki pemandangan alam perdesaan yang cukup menarik ', weight:75, selected:false},
                        {id: 4, name: 'Memiliki pemandangan alam perdesaan yang sangat menarik', weight:100, selected:false}
                    ]
                },
                {id:2, name:'Atraksi', description_entry:'', video_url_1:'https://youtu.be/wxuSNqDVymQ', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/7/400/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/8/400/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/9/400/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/10/400/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/11/400/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/12/400/300'},
                    ],
                    provision:[
                        {id: 5, name:'Tidak memiliki aktivitas wisata', weight:0, selected:false},
                        {id: 6, name: 'Aktivitas wisata tidak menarik dan monoton', weight: 50, selected:false},
                        {id: 7, name: 'Cukup menarik dan memiliki pilihan aktivitas wisata', weight:75, selected:false},
                        {id: 8, name: 'Sangat menarik, memiliki beragam pilihan aktivitas wisata, dan berbeda dengan destinasi wisata lain', weight:100, selected:false}
                    ]
                },
                {id:3, name:'Kedekatan Destinasi', description_entry:'', video_url_1:'https://youtu.be/wxuSNqDVymQ', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/7/400/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/8/400/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/9/400/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/10/400/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/11/400/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/12/400/300'},
                    ],
                    provision:[
                        {id: 9, name:'Tidak memiliki aktivitas wisata', weight:0, selected:false},
                        {id: 10, name: 'Aktivitas wisata tidak menarik dan monoton', weight: 50, selected:false},
                        {id: 11, name: 'Cukup menarik dan memiliki pilihan aktivitas wisata', weight:75, selected:false},
                        {id: 12, name: 'Sangat menarik, memiliki beragam pilihan aktivitas wisata, dan berbeda dengan destinasi wisata lain', weight:100, selected:false}
                    ]
                }
            ]
        },
        {
            id:2, name:'Keterjangkauan / Accessbility', selected:false, aspect:[
                {id:4, name:'Jaringan Jalan', description_entry:'', video_url_1:'https://youtu.be/1QRXNWi60ho', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/1/400/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/2/400/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/3/400/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/4/400/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/5/400/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/6/400/300'},
                    ],
                    provision:[
                        {id: 13, name:'Jalan sangat rusak dan sulit dilewati', weight:0, selected:false},
                        {id: 14, name: 'Kondisi jalan kurang baik, berlubang, namun masih mudah dilewati', weight: 50, selected:false},
                        {id: 15, name: 'Kondisi jalan sempit, namun cukup baik dan mudah dilewati', weight:75, selected:false},
                        {id: 16, name: 'Kondisi jalan sangat baik dan lebar', weight:100, selected:false}
                    ]
                },
                {id:5, name:'Moda Transportasi Umum', description_entry:'', video_url_1:'https://youtu.be/WTNIiRpYSoY', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/1/400/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/2/400/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/3/400/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/4/400/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/5/400/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/6/400/300'},
                    ],
                    provision:[
                        {id: 16, name:'Tidak ada angkutan umum', weight:0, selected:false},
                        {id: 17, name: 'Angkutan umum terbatas dan jarang melintas', weight: 50, selected:false},
                        {id: 18, name: 'Cukup tersedia angkutan umum dan sering melintas', weight:75, selected:false},
                        {id: 19, name: 'Tersedia banyak angkutan umum dan sering melintas', weight:100, selected:false}
                    ]
                }
            ]
        },
        {
            id:3, name:'Fasilitas Pendukung / Amenity', selected:false, aspect:[
                {id:6, name:'Jaringan Jalan', description_entry:'', video_url_1:'https://youtu.be/1QRXNWi60ho', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/1/400/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/2/400/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/3/400/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/4/400/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/5/400/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/6/400/300'},
                    ],
                    provision:[
                        {id: 20, name:'Jalan sangat rusak dan sulit dilewati', weight:0, selected:false},
                        {id: 21, name: 'Kondisi jalan kurang baik, berlubang, namun masih mudah dilewati', weight: 50, selected:false},
                        {id: 22, name: 'Kondisi jalan sempit, namun cukup baik dan mudah dilewati', weight:75, selected:false},
                        {id: 23, name: 'Kondisi jalan sangat baik dan lebar', weight:100, selected:false}
                    ]
                },
                {id:7, name:'Moda Transportasi Umum', description_entry:'', video_url_1:'https://youtu.be/WTNIiRpYSoY', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/1/400/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/2/400/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/3/400/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/4/400/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/5/400/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/6/400/300'},
                    ],
                    provision:[
                        {id: 24, name:'Tidak ada angkutan umum', weight:0, selected:false},
                        {id: 25, name: 'Angkutan umum terbatas dan jarang melintas', weight: 50, selected:false},
                        {id: 26, name: 'Cukup tersedia angkutan umum dan sering melintas', weight:75, selected:false},
                        {id: 27, name: 'Tersedia banyak angkutan umum dan sering melintas', weight:100, selected:false}
                    ]
                }
            ]
        }
    ]
}

const ProposalReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_FINAL_RESULT: 
            return{
                ...state,
                final_result: action.payload
            }
        case UPDATE_PENILAI_PROFILE:
            return{
                ...state,
                penilai_profile: action.payload
            }
        case UPDATE_SUBMISSION_PROFILE:
            return{
                ...state,
                submission_profile: action.payload
            }
        case UPDATE_MASTER_DATA_CRITERIA:
            return{
                ...state,
                criteria: action.payload
            }
        case SELECT_CRITERIA:
            const newData = state.criteria.map(item => item.id == action.payload ? {...item, selected: item.selected == true ? false : true} : {...item, selected: false})
            return{
                ...state,
                criteria: newData
            }
        case SELECT_PROVISION:
            let aspectData = []
            const newDataProvision = state.criteria.map(item => {
                if(item.id == action.payload.criteriaId){ 
                    item.aspect.map(itemAspect => {
                        if(itemAspect.id == action.payload.aspectId){
                            let tempDataProvision = []
                            itemAspect.provision.map(provision => {
                                if(provision.id === action.payload.provisionId){
                                    tempDataProvision.push({...provision, selected:true})
                                }else{
                                    tempDataProvision.push({...provision, selected:false})
                                }
                            })
                            const aspectDataPush = {...itemAspect, provision: tempDataProvision}
                            aspectData.push(aspectDataPush)
                        }else{
                            aspectData.push({...itemAspect})
                        }
                    })
                    return {...item, aspect:aspectData}
                }else{
                    return {...item}
                }
            })
            return{
                ...state,
                criteria: newDataProvision
            }
        case ENTRY_DESCRIPTION:
            let aspectDataEntry = []
            const newDataEntryDescription = state.criteria.map(item => {
                if(item.id == action.payload.criteriaId){
                    item.aspect.map(aspect => {
                        if(aspect.id == action.payload.aspectId){
                            aspectDataEntry.push({...aspect, description_entry: action.payload.value})
                        }else{
                            aspectDataEntry.push({...aspect})
                        }
                    })
                    return {...item, aspect:aspectDataEntry}
                }else{
                    return {...item}
                }
            })
            return{
                ...state,
                criteria: newDataEntryDescription
            }
        case CHANGE_VIDEO_URL:
            let aspectDataVideo = []
            const newDataVideoUrl = state.criteria.map(item => {
                if(item.id == action.payload.criteriaId){
                    item.aspect.map(aspect => {
                        if(aspect.id == action.payload.aspectId){
                            if(action.payload.type == 1){
                                aspectDataVideo.push({...aspect, video_url_1: action.payload.value})
                            }else{
                                aspectDataVideo.push({...aspect, video_url_2: action.payload.value})
                            }
                        }else{
                            aspectDataVideo.push({...aspect})
                        }
                    })
                    return {...item, aspect:aspectDataVideo}
                }else{
                    return {...item}
                }
            })
            return{
                ...state,
                criteria: newDataVideoUrl
            }
        case INSERT_PROVISION_DATA:
            const incomingData = {criteria_id:action.payload.criteriaId, aspect_id:action.payload.aspectId, provision_id:action.payload.provisionId}
            const dataFilter = state.provision_data.filter(item => item.criteria_id == action.payload.criteriaId && item.aspect_id == action.payload.aspectId)
            const newInsertProvisionData = dataFilter.length == 0 ? [...state.provision_data, incomingData] : [...state.provision_data]
            return{
                ...state,
                provision_data: newInsertProvisionData
            }
        case SET_VALIDATE_ALLOW:
            return{
                ...state,
                validate: true
            }
        case SET_VALIDATE_DENY:
            return{
                ...state,
                validate: false
            }
        case UPDATE_PROVISION_DATA:
            const newDataProvisionTemp = [];
            state.criteria.map(criteria => {
                criteria.aspect.map(aspect => {
                    aspect.provision.map(provision => {
                        if(provision.selected){
                            return newDataProvisionTemp.push({criteria_id: criteria.id, aspect_id: aspect.id, provision_id: provision.id})
                        }
                    })
                })
            })
            return{
                ...state,
                provision_data: newDataProvisionTemp
            }
        case UPDATE_ASPECT_DATA:
            const aspectDataValueTemp = []
            state.criteria.map(criteria => {
                criteria.aspect.map(aspect => {
                    if(aspect.description_entry || aspect.video_url_1 || aspect.video_url_2){
                        return aspectDataValueTemp.push({
                            criteria_id: criteria.id,
                            aspect_id: aspect.id,
                            description: aspect.description_entry,
                            video_url_1: aspect.video_url_1,
                            video_url_2: aspect.video_url_2
                        })
                    }
                })
            })
            return{
                ...state,
                aspect_data_value: aspectDataValueTemp
            }
        default:
            return state;
    }
}

export default ProposalReducer