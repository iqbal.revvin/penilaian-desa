
export const SELECT_ASPECT = 'SELECT-ASPECT'
export const SELECT_PROVISION = 'SELECT-PROVISION'
export const ENTRY_DESCRIPTION = 'ENTRY_DESCRIPTION'
export const CHANGE_IMAGE = 'CHANGE_IMAGE'
export const CHANGE_VIDEO_URL = 'CHANGE_VIDEO_URL'
export const INSERT_PROVISION_DATA = 'INSERT_PROVISION_DATA'
export const SET_VALIDATE_ALLOW = 'SET-VALIDATE-ALLOW'
export const SET_VALIDATE_DENY = 'SET-VALIDATE-DENY'

const initialState = {
    validate:false,
    provision_data : [],
    aspect: [
        {
            id:1, name:'Daya Tarik / Attraction', selected:false, criteria:[
                {id:1, name:'Pemandangan', description_entry:'Hello', video_url_1:'https://youtu.be/Cn0zoLM_oT8', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/1/425/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/2/425/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/3/425/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/4/425/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/5/425/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/6/425/300'},
                    ],
                    provision:[
                        {id: 1, name:'Tidak memiliki pemandangan alam perdesaan yang sangat menarik', weight:0, selected:false},
                        {id: 2, name: 'Memiliki pemandangan alam perdesaan yang kurang menarik', weight: 50, selected:false},
                        {id: 3, name: 'Memiliki pemandangan alam perdesaan yang cukup menarik ', weight:75, selected:false},
                        {id: 4, name: 'Memiliki pemandangan alam perdesaan yang sangat menarik', weight:100, selected:false}
                    ]
                },
                {id:2, name:'Atraksi', description_entry:'', video_url_1:'https://youtu.be/wxuSNqDVymQ', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/7/400/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/8/400/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/9/400/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/10/400/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/11/400/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/12/400/300'},
                    ],
                    provision:[
                        {id: 5, name:'Tidak memiliki aktivitas wisata', weight:0, selected:false},
                        {id: 6, name: 'Aktivitas wisata tidak menarik dan monoton', weight: 50, selected:false},
                        {id: 7, name: 'Cukup menarik dan memiliki pilihan aktivitas wisata', weight:75, selected:false},
                        {id: 8, name: 'Sangat menarik, memiliki beragam pilihan aktivitas wisata, dan berbeda dengan destinasi wisata lain', weight:100, selected:false}
                    ]
                },
                {id:3, name:'Kedekatan Destinasi', description_entry:'', video_url_1:'https://youtu.be/wxuSNqDVymQ', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/7/400/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/8/400/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/9/400/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/10/400/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/11/400/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/12/400/300'},
                    ],
                    provision:[
                        {id: 9, name:'Tidak memiliki aktivitas wisata', weight:0, selected:false},
                        {id: 10, name: 'Aktivitas wisata tidak menarik dan monoton', weight: 50, selected:false},
                        {id: 11, name: 'Cukup menarik dan memiliki pilihan aktivitas wisata', weight:75, selected:false},
                        {id: 12, name: 'Sangat menarik, memiliki beragam pilihan aktivitas wisata, dan berbeda dengan destinasi wisata lain', weight:100, selected:false}
                    ]
                }
            ]
        },
        {
            id:2, name:'Keterjangkauan / Accessbility', selected:false, criteria:[
                {id:4, name:'Jaringan Jalan', description_entry:'', video_url_1:'https://youtu.be/1QRXNWi60ho', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/1/400/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/2/400/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/3/400/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/4/400/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/5/400/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/6/400/300'},
                    ],
                    provision:[
                        {id: 13, name:'Jalan sangat rusak dan sulit dilewati', weight:0, selected:false},
                        {id: 14, name: 'Kondisi jalan kurang baik, berlubang, namun masih mudah dilewati', weight: 50, selected:false},
                        {id: 15, name: 'Kondisi jalan sempit, namun cukup baik dan mudah dilewati', weight:75, selected:false},
                        {id: 16, name: 'Kondisi jalan sangat baik dan lebar', weight:100, selected:false}
                    ]
                },
                {id:5, name:'Moda Transportasi Umum', description_entry:'', video_url_1:'https://youtu.be/WTNIiRpYSoY', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/1/400/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/2/400/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/3/400/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/4/400/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/5/400/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/6/400/300'},
                    ],
                    provision:[
                        {id: 16, name:'Tidak ada angkutan umum', weight:0, selected:false},
                        {id: 17, name: 'Angkutan umum terbatas dan jarang melintas', weight: 50, selected:false},
                        {id: 18, name: 'Cukup tersedia angkutan umum dan sering melintas', weight:75, selected:false},
                        {id: 19, name: 'Tersedia banyak angkutan umum dan sering melintas', weight:100, selected:false}
                    ]
                }
            ]
        },
        {
            id:3, name:'Fasilitas Pendukung / Amenity', selected:false, criteria:[
                {id:6, name:'Jaringan Jalan', description_entry:'', video_url_1:'https://youtu.be/1QRXNWi60ho', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/1/400/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/2/400/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/3/400/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/4/400/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/5/400/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/6/400/300'},
                    ],
                    provision:[
                        {id: 20, name:'Jalan sangat rusak dan sulit dilewati', weight:0, selected:false},
                        {id: 21, name: 'Kondisi jalan kurang baik, berlubang, namun masih mudah dilewati', weight: 50, selected:false},
                        {id: 22, name: 'Kondisi jalan sempit, namun cukup baik dan mudah dilewati', weight:75, selected:false},
                        {id: 23, name: 'Kondisi jalan sangat baik dan lebar', weight:100, selected:false}
                    ]
                },
                {id:7, name:'Moda Transportasi Umum', description_entry:'', video_url_1:'https://youtu.be/WTNIiRpYSoY', video_url_2:'',
                    images:[
                        {id: 1, name:'image_1', url: 'https://picsum.photos/id/1/400/300'},
                        {id: 2, name:'image_1', url: 'https://picsum.photos/id/2/400/300'},
                        {id: 3, name:'image_1', url: 'https://picsum.photos/id/3/400/300'},
                        {id: 4, name:'image_1', url: 'https://picsum.photos/id/4/400/300'},
                        {id: 5, name:'image_1', url: 'https://picsum.photos/id/5/400/300'},
                        {id: 6, name:'image_1', url: 'https://picsum.photos/id/6/400/300'},
                    ],
                    provision:[
                        {id: 24, name:'Tidak ada angkutan umum', weight:0, selected:false},
                        {id: 25, name: 'Angkutan umum terbatas dan jarang melintas', weight: 50, selected:false},
                        {id: 26, name: 'Cukup tersedia angkutan umum dan sering melintas', weight:75, selected:false},
                        {id: 27, name: 'Tersedia banyak angkutan umum dan sering melintas', weight:100, selected:false}
                    ]
                }
            ]
        }
    ]
}

const ProposalReducer = (state = initialState, action) => {
    switch (action.type) {
        case SELECT_ASPECT:
            const newData = state.aspect.map(item => item.id == action.payload ? {...item, selected: item.selected == true ? false : true} : {...item, selected: false})
            return{
                ...state,
                aspect: newData
            }
        case SELECT_PROVISION:
            let criteriaData = []
            const newDataProvision = state.aspect.map(item => {
                if(item.id == action.payload.aspectId){ 
                    item.criteria.map(itemCriteria => {
                        if(itemCriteria.id == action.payload.criteriaId){
                            let tempDataProvision = []
                            itemCriteria.provision.map(provision => {
                                if(provision.id === action.payload.provisionId){
                                    tempDataProvision.push({...provision, selected:true})
                                }else{
                                    tempDataProvision.push({...provision, selected:false})
                                }
                            })
                            const criteriaDataPush = {...itemCriteria, provision: tempDataProvision}
                            criteriaData.push(criteriaDataPush)
                        }else{
                            criteriaData.push({...itemCriteria})
                        }
                    })
                    return {...item, criteria:criteriaData}
                }else{
                    return {...item}
                }
            })
            return{
                ...state,
                aspect: newDataProvision
            }
        case ENTRY_DESCRIPTION:
            let criteriaDataEntry = []
            const newDataEntryDescription = state.aspect.map(item => {
                if(item.id == action.payload.aspectId){
                    item.criteria.map(criteria => {
                        if(criteria.id == action.payload.criteriaId){
                            criteriaDataEntry.push({...criteria, description_entry: action.payload.value})
                        }else{
                            criteriaDataEntry.push({...criteria})
                        }
                    })
                    return {...item, criteria:criteriaDataEntry}
                }else{
                    return {...item}
                }
            })
            return{
                ...state,
                aspect: newDataEntryDescription
            }
        case CHANGE_VIDEO_URL:
            let criteriaDataVideo = []
            const newDataVideoUrl = state.aspect.map(item => {
                if(item.id == action.payload.aspectId){
                    item.criteria.map(criteria => {
                        if(criteria.id == action.payload.criteriaId){
                            if(action.payload.type == 1){
                                criteriaDataVideo.push({...criteria, video_url_1: action.payload.value})
                            }else{
                                criteriaDataVideo.push({...criteria, video_url_2: action.payload.value})
                            }
                        }else{
                            criteriaDataVideo.push({...criteria})
                        }
                    })
                    return {...item, criteria:criteriaDataVideo}
                }else{
                    return {...item}
                }
            })
            return{
                ...state,
                aspect: newDataVideoUrl
            }
        case INSERT_PROVISION_DATA:
            const incomingData = {aspect_id:action.payload.aspectId, criteria_id:action.payload.criteriaId, provision_id:action.payload.provisionId}
            const dataFilter = state.provision_data.filter(item => item.aspect_id == action.payload.aspectId && item.criteria_id == action.payload.criteriaId)
            const newInsertProvisionData = dataFilter.length == 0 ? [...state.provision_data, incomingData] : [...state.provision_data]
            return{
                ...state,
                provision_data: newInsertProvisionData
            }
        case SET_VALIDATE_ALLOW:
            return{
                ...state,
                validate: true
            }
        case SET_VALIDATE_DENY:
            return{
                ...state,
                validate: false
            }
        default:
            return state;
    }
}

export default ProposalReducer