import {combineReducers} from 'redux'
import ProposalReducer from './ProposalReducer'


const reducer = combineReducers({
    proposal: ProposalReducer
})

export default reducer