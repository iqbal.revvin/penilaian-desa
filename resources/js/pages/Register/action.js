import API, { REGISTER, SEKOLAH_SEARCH } from "../../services/api/ApiContainer";

export const getApiSchoolSearch = async (q) => {
    try {
        const response = await API.get(`${SEKOLAH_SEARCH}/?q=${q}`);
        return response
    } catch (error) {
        return error
    }
}

export const getApiRegister = async (data) => {
    try {
        const response = await API.post(REGISTER, data)
        return response
    } catch (error) {
        return error        
    }
}