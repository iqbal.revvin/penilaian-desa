import ReactDOM from 'react-dom'
import { Persistor, Store } from '../../services/Redux/store'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import ProposalPage from './ProposalPage'

const rootElement = document.getElementById('proposal-submission')
if(rootElement){
    const userId = rootElement.getAttribute('userId')
    ReactDOM.render(
        <Provider store={Store}>
            <PersistGate loading={null} persistor={Persistor}>
                <ProposalPage userId={userId} />
            </PersistGate>
        </Provider>,
        rootElement
    )
}