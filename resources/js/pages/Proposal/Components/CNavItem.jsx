import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native-web';
import CText from '../../../components/Text/CText';
import { iconDown } from '../../../assets/images/icons'

const CNavItem = ({ active, label, onSelect }) => {
    const styles = styling(active)
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.collapItem} onPress={onSelect}>
                <View style={{ flex: 1, alignItems: 'center', backgroundColor: 'transparent' }}>
                    <CText bold style={{ fontSize: 16, color: 'white' }}>
                        {label}
                    </CText>
                </View>
            </TouchableOpacity>
        </View>
    );
}

CNavItem.defaultProps = {
    active: false
}

export default CNavItem;

const styling = (active) => StyleSheet.create({
    container: {
        marginVertical:1
    },
    collapItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: 35,
        backgroundColor: active ? 'teal' : 'royalblue',
        boxShadow: `${5}px ${5}px ${5}px rgba(95,101,255,0.17)`
    }
})

