import React from 'react';
import { View, StyleSheet } from 'react-native-web';

const CVideoItem = ({ embedId }) => {
    return (
        <View style={styles.container}>
            <View style={{ width:200, height:140 }}>
                <iframe src={`https://www.youtube.com/embed/${embedId}`}
                    className='shadow-lg rounded-md object-contain w-full md:h-auto lg:h-[140px] h-[175px]'
                    title="YouTube video player"
                    frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen />
            </View>
        </View>
    );
}

export default CVideoItem;

const styles = StyleSheet.create({
    container:{

    }
})

