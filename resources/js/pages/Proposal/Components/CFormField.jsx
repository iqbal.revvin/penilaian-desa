import React from 'react';
import { View, StyleSheet } from 'react-native-web';
import CText from '../../../components/Text/CText';
import CTextInput from '../../../components/TextInput/CTextInput';

const CFormField = ({label, placeholder, value, onChangeText, disabled}) => {
    return (
        <View style={styles.container}>
            <View style={styles.labelContainer}>
                <CText>{label}</CText>
            </View>
            <View style={styles.inputContainer}>
                <CTextInput value={value} placeholder={placeholder} onChangeText={onChangeText} disabled={disabled} />
            </View>
        </View>
    );
}

export default React.memo(CFormField);

const styles = StyleSheet.create({
    container: {
        padding: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    labelContainer: {
        backgroundColor: 'transparent',
        width: '20%'
    },
    inputContainer: {
        backgroundColor: 'transparent',
        width: '70%',
        marginLeft: 10
    }
})

