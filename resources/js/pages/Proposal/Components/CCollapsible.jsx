import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native-web';
import CText from '../../../components/Text/CText';
import { iconDown } from '../../../assets/images/icons'

const CCollapsible = ({ active, label, onPress, children }) => {
    const styles = styling(active)
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPress}>
                <View style={styles.collapItem}>
                    <View style={{ flex: 1, alignItems: 'center', marginLeft: 50, backgroundColor: 'transparent' }}>
                        <CText bold style={{ fontSize: 16, color: 'white' }}>
                            {label}
                        </CText>
                    </View>
                    <View style={{ backgroundColor: 'transparent', paddingRight: 20 }}>
                        <Image source={iconDown} style={styles.iconArrowDownSection} />
                    </View>
                </View>
            </TouchableOpacity>
            <View style={{ display: active ? 'flex' : 'none' }}>
                {children}
            </View>
        </View>
    );
}

CCollapsible.defaultProps = {
    active: false
}

export default CCollapsible;

const styling = (active) => StyleSheet.create({
    container: {

    },
    collapItem: {
        flexDirection: 'row',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: 40,
        backgroundColor: active ? 'teal' : 'royalblue',
        boxShadow: `${5}px ${5}px ${5}px rgba(95,101,255,0.17)`
    },
    iconArrowDownSection: {
        width: 20,
        height: 20,
        tintColor: 'white'
    }
})

