import React, { Component, Fragment } from 'react';
import CNavbarAuth from '../../components/Navbars/CNavbarAuth';
// import "@fortawesome/fontawesome-free/css/all.min.css";
import CHeroSection from './components/CHeroSection';
import CSectionFuture from './components/CSectionFuture';

class Homepage extends Component {
    render() {
        return (
            <Fragment>
                <CNavbarAuth transparent />
                <main>
                    <div className="relative pt-16 pb-32 flex content-center items-center justify-center min-h-screen-75">
                        <CHeroSection />
                    </div>
                    <section className="pb-20 bg-blueGray-200 -mt-24">
                        <div className="container mx-auto px-4">
                            <CSectionFuture />
                        </div>
                    </section>
                </main>
            </Fragment>
        );
    }
}

export default Homepage;