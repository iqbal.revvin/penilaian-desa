import React, { Component } from 'react'
import { StyleSheet, View, ScrollView } from 'react-native-web'
import CText from '../../components/Text/CText'
import { connect } from 'react-redux'
import {
    INSERT_PROVISION_DATA, SET_VALIDATE_ALLOW,
    SET_VALIDATE_DENY, UPDATE_ASPECT_DATA, UPDATE_MASTER_DATA_CRITERIA, UPDATE_PROVISION_DATA, UPDATE_SUBMISSION_PROFILE,
    UPDATE_PENILAI_PROFILE,
    UPDATE_FINAL_RESULT
} from '../../services/Redux/reducers/ProposalReducer'
import CButtonRegular from '../../components/Buttons/CButtonRegular'
import CLoading from '../../components/Loaders/CLoading'
import CModalPopup from '../../components/Modals/CModalPopup'
import { iconWarning, iconDocValidate } from '../../assets/images/icons'
import { getQuestionerProposal, insertAspect, insertProvision, validatePenilai } from './services/actions'
import CFieldInfo from './Components/CFieldInfo'
import CTableResult from './Components/CTableResult'
import CIndicatorInfo from './Components/CIndicatorInfo'
import CTableFinalResult from './Components/CTableFinalResult'
import CValueResult from './Components/CValueResult'
import { printIcon } from '../../assets/images/icons'
class ContentResult extends Component {
    constructor() {
        super()
        this.state = {
            isLoadingPage: false,
            loadingMessage: 'Mohon Tunggu',
            count: 0,
            active: false,
            autosave: false,
            isShowPopup: false,
            popupConfig: false,
            validate: false,
            provision_data: [],
            aspect_data_value: [],
            submission_profile: {
                nama_desa_wisata: '',
                kecamatan: '',
                desa: ''
            },
            criteria: [],
        }
        this.pathUrl = window.location.pathname
        this.urlSplit = this.pathUrl?.split('/')
        this.userPenilaiId = this.urlSplit[3]
        this.userDesaId = this.urlSplit[4]
        this.proposalId = this.urlSplit[5]

        this.queryString = window.location.search;
        this.url = new URLSearchParams(this.queryString)
        this.paramUrl = this.url.get('type')
    }

    componentDidMount() {
        this._handleCheckInQuestioner()
        setTimeout(() => {
            this.props.UpdateProvisionData()
        }, 1000)
        setTimeout(() => {
            this._handleValidateCheck()
        }, 1200)
    }

    componentDidUpdate() {

    }

    _handleCheckInQuestioner = () => {
        // console.log(JSON.stringify(pathUrl))
        if (isNaN(this.userPenilaiId) || isNaN(this.userDesaId) || isNaN(this.proposalId)) {
            this.setState({
                isLoadingPage: true,
                isShowPopup: true,
                popupConfig: {
                    icon: iconWarning,
                    title: 'Akses Tidak Valid',
                    message: 'Pastikan anda mengakses data pengisian nilai poposal dengan cara yang valid!',
                    colorButtonPrimary: `teal`,
                    buttonPrimaryText: 'TUTUP',
                    buttonPrimaryAction: () => window.location.replace('/admin'),
                    buttonSecondaryAction: () => window.location.replace('/admin'),
                }
            })
        } else {
            if (this.props.userLoggedIn !== this.userPenilaiId) {
                this.setState({
                    isLoadingPage: true,
                    isShowPopup: true,
                    popupConfig: {
                        icon: iconWarning,
                        title: 'Akses Tidak Valid',
                        message: 'Pastikan anda mengakses penilaian dengan cara yang valid!',
                        colorButtonPrimary: `teal`,
                        buttonPrimaryText: 'TUTUP',
                        buttonPrimaryAction: () => window.location.replace('/admin'),
                        buttonSecondaryAction: () => window.location.replace('/admin'),
                    }
                })
            } else {
                this._handleFetchQuestionerProposal(this.userPenilaiId, this.userDesaId, this.proposalId, 'result')
            }
        }
    }

    _handleFetchQuestionerProposal = async (userPenilaiId, userDesaId, proposalId, type) => {
        this.setState({ isLoadingPage: true, loadingMessage: 'Memuat Data Questioner Proposal' })
        try {
            const response = await getQuestionerProposal(userPenilaiId, userDesaId, proposalId, type)
            if (response.data?.success) {
                this.setState({ isLoadingPage: false })
                this.props.UpdataMasterDataCriteria(response.data.output_data.questioner)
                this.props.UpdatePenilaiProfile(response.data.output_data.penilai_profile)
                this.props.UpdateSubmissionProfile(response.data.output_data.submission_profile)
                this.props.UpdateFinalResult(response.data.output_data.final_result)
            } else {
                let icon = ''
                let title = ''
                let message = ''

                switch (response.data.message) {
                    case 'proposal-for-evaluator':
                        icon = iconWarning
                        title = 'Akses Tidak Valid'
                        message = 'Mohon maaf, anda tidak memiliki akses untuk menilai proposal'
                        break;
                    case 'proposal-not-ready':
                        icon = iconWarning
                        title = 'Proposal Belum Selesai'
                        message = 'Mohon maaf, proposal masih dalam status belum siap untuk di nilai'
                        break;
                    default:
                        icon = iconWarning
                        title = 'Terjadi Masalah'
                        message = 'Mohon maaf, terjadi masalah saat mengakses data questioner proposal'
                        break;
                }

                this.setState({
                    isLoadingPage: true,
                    isShowPopup: true,
                    popupConfig: {
                        icon: icon,
                        title: title,
                        message: message,
                        colorButtonPrimary: `teal`,
                        buttonPrimaryText: 'TUTUP',
                        buttonPrimaryAction: () => window.location.replace('/admin/proposal'),
                        buttonSecondaryAction: () => window.location.replace('/admin/proposal'),
                    }
                })
            }
        } catch (error) {
            this.setState({
                isLoadingPage: true,
                isShowPopup: true,
                popupConfig: {
                    icon: iconWarning,
                    title: 'Terjadi Masalah',
                    message: 'Mohon maaf, terjadi masalah saat mengakses data questioner proposal',
                    colorButtonPrimary: `teal`,
                    buttonPrimaryText: 'TUTUP',
                    buttonPrimaryAction: () => window.location.replace('/admin'),
                    buttonSecondaryAction: () => window.location.replace('/admin'),
                }
            })
        }
    }

    _handleValidateCheck = () => {
        const { proposalState: { criteria, provision_data, validate }, SetValidateAllow } = this.props
        const totalCriteria = criteria.length
        let totalAspect = 0
        criteria.forEach(item => {
            totalAspect += item.aspect.length
        })
        if (provision_data.length == totalAspect) {
            SetValidateAllow()
        }
    }

    _handleUpdateDataToServer = async () => {

        const { provision_data, aspect_data_value, final_result } = this.props.proposalState
        const dataInsertAspectValue = aspect_data_value.map(item => {
            return {
                user_id: this.userDesaId,
                proposal_id: this.proposalId,
                aspect_id: item.aspect_id,
                description: item.description,
                video_url_1: item.video_url_1,
                video_url_2: item.video_url_2
            }
        })
        const dataInsertProvision = provision_data.map(item => {
            return {
                user_id: this.userDesaId,
                proposal_id: this.proposalId,
                aspect_id: item.aspect_id,
                provision_id: item.provision_id
            }
        })
        const _sumValue = (dataValue) => {
            let sumResult = 0;
            dataValue && dataValue.map(item => {
                sumResult += item.aspect
            })
            return sumResult
        }
        const dataInsertFinalScore = {
            penilai_id: this.userPenilaiId,
            attraction: final_result[0].aspect,
            accessbility: final_result[1].aspect,
            amenity: final_result[2].aspect,
            anciliary: final_result[3].aspect,
            promotion: final_result[4].aspect,
            final_score: _sumValue(final_result)
        }
        // console.log('Final Result Send', dataInsertFinalScore)
        const response_provision = insertProvision({ provisions: dataInsertProvision })
        const response_aspect = insertAspect({ aspects: dataInsertAspectValue })
        const response_validate_penilai = validatePenilai(this.userPenilaiId, this.proposalId, dataInsertFinalScore)
        const response = await Promise.all([response_provision, response_aspect, response_validate_penilai])
        // console.log('test response promise all', response)
        if (response[0]?.data?.success || response[1]?.data?.success || response[3]?.data?.success) {
            this.setState({ autosave: true })
            setTimeout(() => {
                this.setState({
                    autosave: false
                })
            }, 1000)
        }
    }

    _handleConfirmValidateProposal = () => {
        this.setState({
            isShowPopup: true,
            popupConfig: {
                icon: iconWarning,
                title: 'Validasi Penilaian',
                message: 'Dengan memvalidasi penilaian, hasil tinjauan penilaian anda di catat ke basis data dan tidak dapat di rubah kembali, apakah anda yakin?.',
                colorButtonPrimary: `teal`,
                buttonPrimaryText: 'VALIDASI PENILAIAN',
                buttonSecondaryText: 'BATALKAN',
                buttonPrimaryAction: this._handleValidatePenilai,
                buttonSecondaryAction: () => this.setState({ isShowPopup: false }),
            }
        })
    }

    _handleValidatePenilai = async () => {
        this.setState({ isLoadingPage: true, isShowPopup: false, loadingMessage: 'Mengirim & Memvalidasi Nilai' })
        this._handleUpdateDataToServer()
        setTimeout(() => {
            this.setState({
                isShowPopup: true,
                popupConfig: {
                    icon: iconDocValidate,
                    title: 'Terimakasih',
                    message: 'Terimakasih, evaluasi dan penilaian proposal anda berhasil tercatat ke basis data.',
                    colorButtonPrimary: `teal`,
                    buttonPrimaryText: 'TUTUP',
                    buttonPrimaryAction: () => window.location.replace('/admin/proposal'),
                    buttonSecondaryAction: () => window.location.replace('/admin/proposal'),
                }
            })
        }, 3000)

        // this.setState({ isShowPopup: false, isLoadingPage: true, loadingMessage: 'Mengirim & Memvalidasi Data Proposal' })
        // this._handleUpdateDataToServer()
        // setTimeout(async () => {
        //     const response = await submitProposal(this.userId, this.proposalId)
        //     if (response?.data?.success) {
        //         this.setState({
        //             isShowPopup: true,
        //             popupConfig: {
        //                 icon: iconDocValidate,
        //                 title: 'Terimakasih',
        //                 message: 'Terimakasih, proposal anda berhasil terkirim dan dilanjutkan kebagian penilai.',
        //                 colorButtonPrimary: `teal`,
        //                 buttonPrimaryText: 'TUTUP',
        //                 buttonPrimaryAction: () => window.location.replace('/admin/proposal'),
        //                 buttonSecondaryAction: () => window.location.replace('/admin/proposal'),
        //             }
        //         })
        //     } else {
        //         alert('Gagal Memvalidasi, pastikan koneksi internet anda baik!')
        //         window.location.reload();
        //     }
        // }, 5000)
    }

    _handleValidateProposal = () => {
        if (this.props.proposalState.validate) {
            this._handleConfirmValidateProposal()
        } else {
            alert('Validasi Belum Diizinkan, pastikan semua data ketentuan nilai telah di isi!')
        }
    }

    _valueWeight = (data) => {
        const weight = data.filter(item => item.selected)
        let color = null
        switch (weight[0].weight) {
            case 0:
                color = 'red'
                break;
            case 50:
                color = 'orange'
                break;
            case 75:
                color = 'darkgoldenrod'
                break;
            default:
                color = 'green'
                break;
        }
        return (
            <CText style={{ fontSize: 20, color: color, textDecorationLine: 'underline', borderRadius: 20, boxShadow: `${5}px ${5}px ${5}px rgba(100,101,255,0.19)` }} bold>
                {weight[0].weight}
            </CText>
        )
    }

    render() {
        const { proposalState: { criteria, validate, submission_profile, penilai_profile } } = this.props
        const { autosave, isLoadingPage, loadingMessage, isShowPopup, popupConfig } = this.state
        return (
            <div className='mb-5'>
                {isLoadingPage && <CLoading message={loadingMessage} />}
                {isShowPopup && <CModalPopup popupData={popupConfig} />}
                <div className='antialiased flex flex-row justify-center'>
                    <div className='w-full lg:w-12/12'>
                        <View>
                            <div>
                                <CText bold style={{ fontSize: 18 }}>Data Desa & Wisata</CText>
                            </div>
                            <View style={styles.formDesaContainer}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ marginHorizontal: 10 }}>
                                        <CFieldInfo label='Nama Desa' value={submission_profile.nama_desa} />
                                        <CFieldInfo label='Nama Desa Wisata' value={submission_profile.nama_desa_wisata} />
                                        <CFieldInfo label='Nama Kepala Desa' value={submission_profile.nama_kepala_desa} />
                                        <CFieldInfo label='Email' value={submission_profile.email} />
                                        <CFieldInfo label='No. HP/WA' value={submission_profile.no_hp} />
                                    </View>
                                    <View style={{ marginHorizontal: 10 }}>
                                        <CFieldInfo label='Provinsi' value={submission_profile.provinsi} />
                                        <CFieldInfo label='Kabupaten' value={submission_profile.kabupaten} />
                                        <CFieldInfo label='Kecamatan' value={submission_profile.kecamatan} />
                                        <CFieldInfo label='Kelurahan' value={submission_profile.nama_desa} />
                                        <CFieldInfo label='Jumlah Pengunjung' value={new Intl.NumberFormat().format(submission_profile.jumlah_pengunjung)} />
                                    </View>
                                </View>
                            </View>
                            <hr className='my-3' />
                            {this.paramUrl == 'forprint' && (
                                <div className='print:hidden'>
                                    <CButtonRegular label='Cetak Halaman' color='ghostwhite' icon={printIcon} colorText={'black'} onPress={() => window.print()} />
                                </div>
                            )}
                            {penilai_profile.penilai_validate == 'Pending' && (
                                <View style={styles.validationContainer}>
                                    <CButtonRegular label='Validasi Nilai' disabled={!validate} onPress={this._handleValidateProposal} />
                                </View>
                            )}
                            <View style={{ backgroundColor: 'gold', width: '100%', marginVertical: 25, paddingVertical: 15, alignItems: 'center' }}>
                                <CText bold style={{ fontSize: 20 }}>RINCIAN NILAI</CText>
                            </View>
                            <View style={{ marginHorizontal: 10, alignSelf: 'center' }}>
                                <View style={{ width: 525 }}>
                                    <CTableResult data={this.props.proposalState.criteria[2]} />
                                </View>
                            </View>
                            <ScrollView horizontal contentContainerStyle={{ width: '100%', justifyContent: 'center', transform: [{ scale: 0.92 }] }}>
                                <View style={{ marginHorizontal: 10, height: 550, }}>
                                    <View style={{ width: 525, height: 300 }}>
                                        <CTableResult data={this.props.proposalState.criteria[0]} />
                                    </View>
                                    <View style={{ width: 525, marginTop: 20, height: 250 }}>
                                        <CTableResult data={this.props.proposalState.criteria[1]} />
                                    </View>
                                </View>
                                <View style={{ marginHorizontal: 10 }}>
                                    <View style={{ width: 525, height: 300, }}>
                                        <CTableResult data={this.props.proposalState.criteria[3]} />
                                    </View>
                                    <View style={{ width: 525, marginTop: 20 }}>
                                        <CTableResult data={this.props.proposalState.criteria[4]} />
                                    </View>
                                </View>
                            </ScrollView>

                            <View style={{ backgroundColor: 'gold', width: '100%', marginTop: this.paramUrl == 'forprint' ? 200 : 0, marginVertical: 25, paddingVertical: 15, alignItems: 'center' }}>
                                <CText bold style={{ fontSize: 20 }}>REKAPITULASI AKHIR</CText>
                            </View>
                            <ScrollView horizontal contentContainerStyle={{ width: '100%', justifyContent: 'center' }}>
                                <View style={{ alignItems: 'center' }}>
                                    <View>
                                        <CIndicatorInfo />
                                    </View>
                                    <View style={{ marginTop: 10 }}>
                                        <CValueResult data={this.props.proposalState.final_result} />
                                    </View>
                                </View>
                                <View style={{ marginLeft: 100, marginTop: 20, transform: [{ scale: 1.1 }] }}>
                                    <CTableFinalResult data={this.props.proposalState.final_result} />
                                </View>

                            </ScrollView>
                            {/* {JSON.stringify(this.props.proposalState.final_result)} */}
                        </View>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    proposalState: state.proposal
})

const mapDispatchToProps = (dispatch) => ({
    UpdateFinalResult: (data) => dispatch({ type: UPDATE_FINAL_RESULT, payload: data }),
    UpdatePenilaiProfile: (data) => dispatch({ type: UPDATE_PENILAI_PROFILE, payload: data }),
    UpdateSubmissionProfile: (data) => dispatch({ type: UPDATE_SUBMISSION_PROFILE, payload: data }),
    UpdataMasterDataCriteria: (data) => dispatch({ type: UPDATE_MASTER_DATA_CRITERIA, payload: data }),
    InsertProvisionData: (data) => dispatch({ type: INSERT_PROVISION_DATA, payload: data }),
    SetValidateAllow: () => dispatch({ type: SET_VALIDATE_ALLOW }),
    SetValidateDeny: () => dispatch({ type: SET_VALIDATE_DENY }),
    UpdateProvisionData: () => dispatch({ type: UPDATE_PROVISION_DATA }),
    UpdateAspectDataValue: () => dispatch({ type: UPDATE_ASPECT_DATA })
})

export default connect(mapStateToProps, mapDispatchToProps)(ContentResult)

const styles = StyleSheet.create({
    autoSavingSection: {
        color: 'darkgray',
        position: 'absolute',
        top: -15,
        bottom: 0,
        left: 0,
        right: 0
    },
    cardItem: {
        marginVertical: 5,
        marginTop: 10,
        padding: 15,
        backgroundColor: 'white',
        borderRadius: 5
    },
    formDesaContainer: {
        backgroundColor: 'transparent',
        marginTop: 20
    },
    formSurveyContainer: {
        backgroundColor: 'transparent',
        alignItems: 'center',
        marginTop: 25
    },
    collapContainer: {
        width: '100%',
        marginVertical: 5
    },
    validationContainer: {
        alignItems: 'flex-end',
        marginTop: 10
    }
})
