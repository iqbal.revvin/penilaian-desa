import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, ScrollView } from 'react-native-web'
import CText from '../../components/Text/CText'
import CTextInput from '../../components/TextInput/CTextInput'
import CCollapsible from './Components/CCollapsible'
import CNavItem from './Components/CNavItem'
import { connect } from 'react-redux'
import {
    CHANGE_VIDEO_URL, ENTRY_DESCRIPTION, INSERT_PROVISION_DATA, SELECT_CRITERIA, SELECT_PROVISION, SET_VALIDATE_ALLOW,
    SET_VALIDATE_DENY, UPDATE_ASPECT_DATA, UPDATE_MASTER_DATA_CRITERIA, UPDATE_PROVISION_DATA, UPDATE_SUBMISSION_PROFILE,
    UPDATE_PENILAI_PROFILE
} from '../../services/Redux/reducers/ProposalReducer'
import CSelectItem from './Components/CSelectItem'
import CUploadImage from '../../components/CUploadImage'
import CVideoItem from './Components/CVideoItem'
import CButtonRegular from '../../components/Buttons/CButtonRegular'
import CLoading from '../../components/Loaders/CLoading'
import CModalPopup from '../../components/Modals/CModalPopup'
import { iconWarning, iconDocValidate } from '../../assets/images/icons'
import { getQuestionerProposal, insertAspect, insertProvision } from './services/actions'
import CFieldInfo from './Components/CFieldInfo'

class ContentEvaluate extends Component {
    constructor() {
        super()
        this.state = {
            isLoadingPage: false,
            loadingMessage: 'Mohon Tunggu',
            count: 0,
            active: false,
            autosave: false,
            isShowPopup: false,
            popupConfig: false,
            validate: false,
            provision_data: [],
            aspect_data_value: [],
            submission_profile: {
                nama_desa_wisata: '',
                kecamatan: '',
                desa: ''
            },
            criteria: [],
        }
        this.pathUrl = window.location.pathname
        this.urlSplit = this.pathUrl?.split('/')
        this.userPenilaiId = this.urlSplit[3]
        this.userDesaId = this.urlSplit[4]
        this.proposalId = this.urlSplit[5]
    }

    componentDidMount() {
        this._handleCheckInQuestioner()
        setTimeout(() => {
            this.props.UpdateProvisionData()
        }, 1000)
        setTimeout(() => {
            this._handleValidateCheck()
        }, 1200)
    }

    componentDidUpdate() {

    }

    _handleCheckInQuestioner = () => {
        // console.log(JSON.stringify(pathUrl))
        if (isNaN(this.userPenilaiId) || isNaN(this.userDesaId) || isNaN(this.proposalId)) {
            this.setState({
                isLoadingPage: true,
                isShowPopup: true,
                popupConfig: {
                    icon: iconWarning,
                    title: 'Akses Tidak Valid',
                    message: 'Pastikan anda mengakses data pengisian nilai poposal dengan cara yang valid!',
                    colorButtonPrimary: `teal`,
                    buttonPrimaryText: 'TUTUP',
                    buttonPrimaryAction: () => window.location.replace('/admin'),
                    buttonSecondaryAction: () => window.location.replace('/admin'),
                }
            })
        } else {
            if (this.props.userLoggedIn !== this.userPenilaiId) {
                this.setState({
                    isLoadingPage: true,
                    isShowPopup: true,
                    popupConfig: {
                        icon: iconWarning,
                        title: 'Akses Tidak Valid',
                        message: 'Pastikan anda mengakses penilaian dengan cara yang valid!',
                        colorButtonPrimary: `teal`,
                        buttonPrimaryText: 'TUTUP',
                        buttonPrimaryAction: () => window.location.replace('/admin'),
                        buttonSecondaryAction: () => window.location.replace('/admin'),
                    }
                })
            } else {
                this._handleFetchQuestionerProposal(this.userPenilaiId, this.userDesaId, this.proposalId, 'evaluate')
            }
        }
    }

    _handleFetchQuestionerProposal = async (userPenilaiId, userDesaId, proposalId, type) => {
        this.setState({ isLoadingPage: true, loadingMessage: 'Memuat Data Questioner Proposal' })
        try {
            const response = await getQuestionerProposal(userPenilaiId, userDesaId, proposalId, type)
            if (response.data?.success) {
                this.setState({ isLoadingPage: false })
                this.props.UpdataMasterDataCriteria(response.data.output_data.questioner)
                this.props.UpdatePenilaiProfile(response.data.output_data.penilai_profile)
                this.props.UpdateSubmissionProfile(response.data.output_data.submission_profile)
            } else {
                let icon = ''
                let title = ''
                let message = ''

                switch (response.data.message) {
                    case 'proposal-for-evaluator':
                        icon = iconWarning
                        title = 'Akses Tidak Valid'
                        message = 'Mohon maaf, anda tidak memiliki akses untuk menilai proposal'
                        break;
                    case 'proposal-not-ready':
                        icon = iconWarning
                        title = 'Proposal Belum Selesai'
                        message = 'Mohon maaf, proposal masih dalam status belum siap untuk di nilai'
                        break;
                    case 'proposal-is-evaluate':
                        icon = iconDocValidate
                        title = 'Proposal Tervalidasi'
                        message = 'Mohon maaf, proposal sudah terevaluasi & tervalidasi, terimaksih.'
                        break;
                    default:
                        icon = iconWarning
                        title = 'Terjadi Masalah'
                        message = 'Mohon maaf, terjadi masalah saat mengakses data questioner proposal'
                        break;
                }

                this.setState({
                    isLoadingPage: true,
                    isShowPopup: true,
                    popupConfig: {
                        icon: icon,
                        title: title,
                        message: message,
                        colorButtonPrimary: `teal`,
                        buttonPrimaryText: 'TUTUP',
                        buttonPrimaryAction: () => window.location.replace('/admin/proposal'),
                        buttonSecondaryAction: () => window.location.replace('/admin/proposal'),
                    }
                })
            }
        } catch (error) {
            this.setState({
                isLoadingPage: true,
                isShowPopup: true,
                popupConfig: {
                    icon: iconWarning,
                    title: 'Terjadi Masalah',
                    message: 'Mohon maaf, terjadi masalah saat mengakses data questioner proposal',
                    colorButtonPrimary: `teal`,
                    buttonPrimaryText: 'TUTUP',
                    buttonPrimaryAction: () => window.location.replace('/admin'),
                    buttonSecondaryAction: () => window.location.replace('/admin'),
                }
            })
        }
    }

    _handleValidateCheck = () => {
        const { proposalState: { criteria, provision_data, validate }, SetValidateAllow } = this.props
        const totalCriteria = criteria.length
        let totalAspect = 0
        criteria.forEach(item => {
            totalAspect += item.aspect.length
        })
        if (provision_data.length == totalAspect) {
            SetValidateAllow()
        }
    }

    _handleSelectCriteria = async (id) => {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
        this.props.SelectedCriteria(id)
        this.props.UpdateAspectDataValue()
        this._handleUpdateDataToServer()
    }

    _handleUpdateDataToServer = async () => {
        const { provision_data, aspect_data_value } = this.props.proposalState
        const dataInsertAspectValue = aspect_data_value.map(item => {
            return {
                user_id: this.userDesaId,
                proposal_id: this.proposalId,
                aspect_id: item.aspect_id,
                description: item.description,
                video_url_1: item.video_url_1,
                video_url_2: item.video_url_2
            }
        })
        const dataInsertProvision = provision_data.map(item => {
            return {
                user_id: this.userDesaId,
                proposal_id: this.proposalId,
                aspect_id: item.aspect_id,
                provision_id: item.provision_id
            }
        })
        const response_provision = insertProvision({ provisions: dataInsertProvision })
        const response_aspect = insertAspect({ aspects: dataInsertAspectValue })
        const response = await Promise.all([response_provision, response_aspect])
        // console.log('test response promise all', response)
        if (response[0]?.data?.success || response[1]?.data?.success) {
            this.setState({ autosave: true })
            setTimeout(() => {
                this.setState({
                    autosave: false
                })
            }, 1000)
        }
    }

    _handleSelectProvision = (criteriaId, aspectId, provisionId) => {
        const data = { criteriaId, aspectId, provisionId }
        this.props.SelectedProvision(data)
        this.props.InsertProvisionData(data)
        setTimeout(() => {
            this.props.UpdateProvisionData()
        }, 500)
        setTimeout(() => {
            this._handleValidateCheck()
        }, 1000)
    }


    _handleConfirmValidateProposal = () => {
        this.setState({
            isShowPopup: true,
            popupConfig: {
                icon: iconWarning,
                title: 'Kesimpulan Penilaian',
                message: 'Dengan melakukan kesimpulan, maka anda akan di alihkan ke halaman hasil penilaian akhir dan evaluasi',
                colorButtonPrimary: `teal`,
                buttonPrimaryText: 'LANJUTKAN',
                buttonSecondaryText: 'BATALKAN',
                buttonPrimaryAction: this._handleEvaluateProposal,
                buttonSecondaryAction: () => this.setState({ isShowPopup: false }),
            }
        })
    }

    _handleEvaluateProposal = async () => {
        this.setState({ isShowPopup: false, isLoadingPage: true, loadingMessage: 'Mengalihkan ke halaman validasi penilaian' })
        this._handleUpdateDataToServer()
        setTimeout(() => {
            window.location.replace(`/admin/proposal-evaluate/${this.userPenilaiId}/${this.userDesaId}/${this.proposalId}/result`)
        }, 1000)
    }

    _handleValidateProposal = () => {
        if (this.props.proposalState.validate) {
            this._handleConfirmValidateProposal()
        } else {
            alert('Validasi Belum Diizinkan, pastikan semua data ketentuan nilai telah di isi!')
        }
    }

    _valueWeight = (data) => {
        const weight = data.filter(item => item.selected)
        let color = null
        switch (weight[0].weight) {
            case 0:
                color = 'red'
                break;
            case 50:
                color = 'orange'
                break;
            case 75:
                color = 'darkgoldenrod'
                break;
            default:
                color = 'green'
                break;
        }
        return (
            <CText style={{ fontSize: 20, color: color, textDecorationLine: 'underline', borderRadius: 20, boxShadow: `${5}px ${5}px ${5}px rgba(100,101,255,0.19)` }} bold>
                {weight[0].weight}
            </CText>
        )
    }

    render() {
        const { proposalState: { criteria, validate, submission_profile, penilai_profile } } = this.props
        const { autosave, isLoadingPage, loadingMessage, isShowPopup, popupConfig } = this.state
        return (
            <div className='-mx-6 mb-5 lg:mx-auto'>
                {isLoadingPage && <CLoading message={loadingMessage} />}
                {isShowPopup && <CModalPopup popupData={popupConfig} />}
                <div className='flex flex-col lg:flex-row'>
                    <div className='mb-24 lg:mb-0 lg:w-3/12 hidden lg:flex mr-5'>
                        {/* <CText bold style={{ color: 'grey', fontSize: 18 }}>On Development!!!</CText> */}
                        <div className='bg-blue-200 fixed flex lg:flex-col'>
                            {criteria.map((item, i) => {
                                return (
                                    <div key={i} className='w-64'>
                                        <CNavItem label={item.name} active={item.selected} onSelect={() => this._handleSelectCriteria(item.id)} />
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                    <div className='w-full lg:w-9/12'>
                        <ScrollView>
                            <View style={styles.formDesaContainer}>
                                {/* <CFormField label='Desa Wisata' value={submission_profile.nama_desa_wisata} placeholder='Masukan nama desa wisata' disabled />
                                <CFormField label='Kecamatan' value={submission_profile.kecamatan}  placeholder='Masukan nama kecamatan' disabled />
                                <CFormField label='Desa' value={submission_profile.desa}  placeholder='Masukan nama desa' disabled /> */}
                                <div className='flex flex-col md:flex-row lg:flex-row'>
                                    <View style={{ marginHorizontal: 5 }}>
                                        <CFieldInfo label='Nama Desa' value={submission_profile.nama_desa} />
                                        <CFieldInfo label='Nama Desa Wisata' value={submission_profile.nama_desa_wisata} />
                                        <CFieldInfo label='Nama Kepala Desa' value={submission_profile.nama_kepala_desa} />

                                    </View>
                                    <View style={{ marginHorizontal: 5 }}>
                                        <CFieldInfo label='Nama Penilai' value={penilai_profile.nama_penilai} />
                                        <CFieldInfo label='Asal Instansi' value={penilai_profile.asal_instansi} />
                                        <CFieldInfo label='Jabatan' value={penilai_profile.jabatan} />
                                    </View>
                                </div>
                            </View>
                            <View style={styles.formSurveyContainer}>
                                {autosave && (
                                    <View style={styles.autoSavingSection}>
                                        <CText style={{ color: 'gray' }}>Saving Data...</CText>
                                    </View>
                                )}
                                {criteria.map((criteria, i) => {
                                    return (
                                        <View key={i} style={styles.collapContainer}>
                                            <CCollapsible label={criteria.name} active={criteria.selected} onPress={() => this._handleSelectCriteria(criteria.id)}>
                                                {criteria.aspect.map((aspect, aspectIdx) => {
                                                    const urlVideo = aspect.video_url_1
                                                    const splitUrl = urlVideo?.split('/')
                                                    const urlVideo2 = aspect.video_url_2
                                                    const splitUrl2 = urlVideo2?.split('/')
                                                    return (
                                                        <View key={aspectIdx} style={styles.cardItem}>
                                                            <CText bold>
                                                                Jabarkan secara singkat dari {aspect.name} yang dimiliki
                                                                Desa Wisata Yang Diajukan!
                                                            </CText>
                                                            <CText>Dilengkapi dengan dokumen pendukung berupa foto dan atau video</CText>
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <CText bold>Isian diharapkan misal : </CText>
                                                                <CText>Jenis/Bentuk {aspect.name} yang dimiliki dan lainnya</CText>
                                                            </View>
                                                            <ScrollView horizontal showsHorizontalScrollIndicator={false} >
                                                                <View style={{ marginVertical: 5 }}>
                                                                    {aspect.provision.map((provision, provisionIdx) => {
                                                                        return (
                                                                            <View key={provisionIdx} style={{ marginVertical: 5 }}>
                                                                                <CSelectItem label={provision.name} onSelect={() => this._handleSelectProvision(criteria.id, aspect.id, provision.id)} active={provision.selected} />
                                                                            </View>
                                                                        )
                                                                    })}
                                                                </View>
                                                            </ScrollView>
                                                            <View style={{ margin: 2, height: 100 }}>
                                                                <CTextInput value={aspect.description_entry} multiline numberOfLines={2} placeholder='Deskripsi Singkat'
                                                                />
                                                            </View>
                                                            <View style={{ flexDirection: 'row', alignItems: 'center', width: 150, marginVertical: 25 }}>
                                                                <View style={{ width: '100%', flexDirection: 'row' }}>
                                                                    <CText style={{ fontSize: 18, marginRight: 5 }} bold>
                                                                        Nilai Aspek :
                                                                    </CText>
                                                                    <View>
                                                                        {this._valueWeight(aspect.provision)}
                                                                    </View>
                                                                </View>
                                                            </View>
                                                            {/* <View style={{ margin: 2, marginTop: 5, flexDirection: 'row', alignItems: 'center' }}>
                                                                <CText style={{ marginRight: 10 }}>URL Video Youtube 1: </CText>
                                                                <View style={{ width: '55%' }}>
                                                                    <CTextInput value={aspect.video_url_1} placeholder='Link Video Youtube' />
                                                                </View>
                                                            </View>
                                                            <View style={{ margin: 2, marginTop: 5, flexDirection: 'row', alignItems: 'center' }}>
                                                                <CText style={{ marginRight: 10 }}>URL Video Youtube 2: </CText>
                                                                <View style={{ width: '55%' }}>
                                                                    <CTextInput value={aspect.video_url_2} placeholder='Link Video Youtube 2' />
                                                                </View>
                                                            </View> */}
                                                            <View style={{ marginTop: 10 }}>
                                                                <ScrollView horizontal>
                                                                    {!!urlVideo && <View style={{ marginRight: 10 }}><CVideoItem embedId={splitUrl[3]} /></View>}
                                                                    {!!urlVideo2 && <View style={{ marginRight: 10 }}><CVideoItem embedId={splitUrl2[3]} /></View>}
                                                                    {aspect.images.map((images, imagesIdx) => {
                                                                        return (
                                                                            <View key={imagesIdx} style={{ marginRight: 10 }}>
                                                                                {/* <input type="file" name="myImage" onChange={this._handleChangeImage(images.name, aspect.id)}/> */}
                                                                                {images.url && (
                                                                                    <TouchableOpacity onPress={() => window.open(images.url, '_blank')} style={{ zIndex: 10 }}>
                                                                                        <CUploadImage initSrc={images.url} disabled />
                                                                                    </TouchableOpacity>
                                                                                )}
                                                                            </View>
                                                                        )
                                                                    })}
                                                                </ScrollView>
                                                            </View>
                                                            <br /><hr /><hr />
                                                        </View>
                                                    )
                                                })}
                                            </CCollapsible>
                                        </View>
                                    )
                                })}
                            </View>
                            <View style={styles.validationContainer}>
                                <CButtonRegular label='Kesimpulan' disabled={!validate} onPress={this._handleValidateProposal} />
                            </View>
                        </ScrollView>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    proposalState: state.proposal
})

const mapDispatchToProps = (dispatch) => ({
    UpdatePenilaiProfile: (data) => dispatch({ type: UPDATE_PENILAI_PROFILE, payload: data }),
    UpdateSubmissionProfile: (data) => dispatch({ type: UPDATE_SUBMISSION_PROFILE, payload: data }),
    UpdataMasterDataCriteria: (data) => dispatch({ type: UPDATE_MASTER_DATA_CRITERIA, payload: data }),
    SelectedCriteria: (value) => dispatch({ type: SELECT_CRITERIA, payload: value }),
    SelectedProvision: (data) => dispatch({ type: SELECT_PROVISION, payload: data }),
    EntryDescription: (data) => dispatch({ type: ENTRY_DESCRIPTION, payload: data }),
    EntryVideoUrl: (data) => dispatch({ type: CHANGE_VIDEO_URL, payload: data }),
    InsertProvisionData: (data) => dispatch({ type: INSERT_PROVISION_DATA, payload: data }),
    SetValidateAllow: () => dispatch({ type: SET_VALIDATE_ALLOW }),
    SetValidateDeny: () => dispatch({ type: SET_VALIDATE_DENY }),
    UpdateProvisionData: () => dispatch({ type: UPDATE_PROVISION_DATA }),
    UpdateAspectDataValue: () => dispatch({ type: UPDATE_ASPECT_DATA })
})

export default connect(mapStateToProps, mapDispatchToProps)(ContentEvaluate)

const styles = StyleSheet.create({
    autoSavingSection: {
        color: 'darkgray',
        position: 'absolute',
        top: -15,
        bottom: 0,
        left: 0,
        right: 0
    },
    cardItem: {
        marginVertical: 5,
        marginTop: 10,
        padding: 15,
        backgroundColor: 'white',
        borderRadius: 5
    },
    formDesaContainer: {
        backgroundColor: 'transparent',
        marginTop: 20
    },
    formSurveyContainer: {
        backgroundColor: 'transparent',
        alignItems: 'center',
        marginTop: 25
    },
    collapContainer: {
        width: '100%',
        marginVertical: 5
    },
    validationContainer: {
        alignItems: 'flex-end',
        marginTop: 10
    }
})
