//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native-web';
import CText from '../../../components/Text/CText';

// create a component
const CTableResult = ({data}) => {
    const colorPotensial = (value) => {
        let color = 'white'
        switch (true) {
            case (value >= 0 && value <= 25):
                color = 'red'
                break;
            case (value >= 26 && value <= 50):
                color = 'orange'
                break;
            case (value >= 51 && value <= 75):
                color = 'yellow'
                break;
            default:
                color = 'chartreuse'
                break;
        }
        return {
            backgroundColor: color
        }
    }
    const _valueWeight = (provision) => {
        const selectData = provision.filter(item => item.selected)
        return selectData[0].weight
    }
    const _aspectResultValue = (weightAspectValue, score) => {
        const weightAspectProsentase = weightAspectValue/100
        const scoreResult = score*weightAspectProsentase
        return scoreResult
    }
    const _sumAspect = (dataCompute) => {
        let sumResult = 0;
        dataCompute && dataCompute.map(item => {
            sumResult += _aspectResultValue(item.weight, _valueWeight(item.provision))
        })
        return sumResult
    }

    const _potentialResult = (weightCriteria, sumResult) => {
        const weightProsentase = weightCriteria/100
        const result = sumResult*weightProsentase
        return result.toFixed(2)
    } 
    return (
        <View style={styles.container}>
            {/* {JSON.stringify(data)} */}
            <View style={[styles.colContainer, {backgroundColor:'teal', alignItems: 'center'}]}>
                <View style={[styles.colItemContainer, { width: 525, backgroundColor: 'transparent' }]}>
                    <CText bold style={stylesWeb.colTitleSection}>{data?.name}</CText>
                </View>
                
            </View>
            <View style={styles.colContainer}>
                <View style={[styles.colItemContainer, { width: 225, backgroundColor: 'transparent' }]}>
                    <CText bold style={stylesWeb.colTitleSection}>ASPEK</CText>
                </View>
                <View style={[styles.colItemContainer, { backgroundColor: 'transparent' }]}>
                    <CText bold style={{ ...stylesWeb.colTitleSection, width: 50 }}>
                        BOBOT ASPEK
                    </CText>
                </View>
                <View style={[styles.colItemContainer, { backgroundColor: 'transparent' }]}>
                    <CText bold style={{ ...stylesWeb.colTitleSection }}>
                        SKOR
                    </CText>
                </View>
                <View style={[styles.colItemContainer, { backgroundColor: 'transparent' }]}>
                    <CText bold style={{ ...stylesWeb.colTitleSection, width: 50 }}>
                        NILAI ASPEK
                    </CText>
                </View>
            </View>
            {data?.aspect.map((item,i) => (
                <View key={i} style={[styles.colContainer, { backgroundColor: 'transparent' }]}>
                    <View style={[styles.colItemDataContainer, { width: 225, alignItems: 'flex-start', backgroundColor: 'transparent' }]}>
                        <CText style={{ ...stylesWeb.colTitleData }}>{item.name}</CText>
                    </View>
                    <View style={[styles.colItemDataContainer]}>
                        <CText style={{ ...stylesWeb.colTitleData }}>
                            {item.weight}%
                        </CText>
                    </View>
                    <View style={[styles.colItemDataContainer]}>
                        <CText style={{ ...stylesWeb.colTitleData }}>
                            {_valueWeight(item.provision)}
                        </CText>
                    </View>
                    <View style={[styles.colItemDataContainer, { borderRightWidth: 3 }]}>
                        <CText style={{ ...stylesWeb.colTitleData }}>
                            {_aspectResultValue(item.weight, _valueWeight(item.provision))}
                        </CText>
                    </View>
                </View>
            ))}
            <View style={[styles.colContainer, { backgroundColor: 'transparent' }]}>
                <View style={[styles.colItemDataContainer, { width: 425, backgroundColor: 'transparent', alignItems: 'flex-start' }]}>
                    <CText bold italic style={stylesWeb.colTitleData}>{data?.name}</CText>
                </View>

                <View style={[styles.colItemDataContainer, { borderRightWidth: 3 }]}>
                    <CText bold style={{ ...stylesWeb.colTitleData }}>
                        {_sumAspect(data?.aspect)} 
                    </CText>
                </View>
            </View>

            <View style={[styles.colContainer, { backgroundColor: 'transparent', borderBottomWidth: 3, borderBottomColor: 'brown' }]}>
                <View style={[styles.colItemDataContainer, { width: 425, backgroundColor: 'transparent', alignItems: 'flex-start' }]}>
                    <CText italic bold style={stylesWeb.colTitleData}>Nilai Akhir (x {data?.weight}%)</CText>
                </View>

                <View style={[styles.colItemDataContainer, colorPotensial(_sumAspect(data?.aspect)), { borderRightWidth: 2 }]}>
                    <CText bold style={{ ...stylesWeb.colTitleData }}>
                        {_potentialResult(data?.weight, _sumAspect(data?.aspect))}
                    </CText>
                </View>
            </View>
        </View>
    );
};

//make this component available to the app
export default CTableResult;

// define your styles
const styles = StyleSheet.create({
    container: {

    },
    colContainer: {
        flexDirection: 'row',
        backgroundColor: 'brown',
        alignItems: 'center'
    },
    colItemContainer: {
        padding: 5,
        width: 100,
        alignItems: 'center'
    },
    colItemDataContainer: {
        padding: 5,
        width: 100,
        alignItems: 'center',
        borderLeftWidth: 3,
        borderTopWidth: 3,
        borderBottomColor: 3,
        borderColor: 'brown'
    },
    colorPotensial: (value) => ({
        backgroundColor: 'red'
    })
});

const stylesWeb = {
    colTitleSection: {
        color: 'white',
        textAlign: 'center'
    },
    colTitleData: {
        color: 'black'
    }
}