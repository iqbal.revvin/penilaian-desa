//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native-web';
import CText from '../../../components/Text/CText';

// create a component
const CTableFinalResult = ({ data }) => {
    const _sumValue = (dataCompute) => {
        let sumResult = 0;
        dataCompute && dataCompute.map(item => {
            sumResult += item.aspect
        })
        return sumResult.toFixed(1)
    }


    return (
        <View style={styles.container}>
            {/* {JSON.stringify(data)} */}
            {/* <View style={[styles.colContainer, {backgroundColor:'teal', alignItems: 'center'}]}>
                <View style={[styles.colItemContainer, { width: 525, backgroundColor: 'transparent' }]}>
                    <CText bold style={stylesWeb.colTitleSection}>{data?.name}</CText>
                </View>
                
            </View> */}
            <View style={styles.colContainer}>
                <View style={[styles.colItemContainer, { width: 225, backgroundColor: 'transparent' }]}>
                    <CText bold style={stylesWeb.colTitleSection}>KRITERIA</CText>
                </View>
                <View style={[styles.colItemContainer, { width: 200, backgroundColor: 'transparent' }]}>
                    <CText bold style={{ ...stylesWeb.colTitleSection, width: 100 }}>
                        NILAI KRITERIA
                    </CText>
                </View>

            </View>
            {data.map((item, i) => {
                return (
                    <View key={i} style={[styles.colContainer, { backgroundColor: 'transparent' }]}>
                        <View style={[styles.colItemDataContainer, { width: 225, alignItems: 'flex-start', backgroundColor: 'transparent' }]}>
                            <CText style={{ ...stylesWeb.colTitleData }}>{item.name}</CText>
                        </View>
                        <View style={[styles.colItemDataContainer, { width: 200, borderRightWidth: 3 }]}>
                            <CText style={{ ...stylesWeb.colTitleData }}>
                                {item.aspect}
                            </CText>
                        </View>
                    </View>
                )
            })}
            <View style={[styles.colContainer, { backgroundColor: 'transparent', borderBottomWidth:3, borderBottomColor:'brown' }]}>
                <View style={[styles.colItemDataContainer, { width: 225, alignItems: 'center', backgroundColor: 'transparent' }]}>
                    <CText style={{ ...stylesWeb.colTitleData }} bold>TOTAL</CText>
                </View>
                <View style={[styles.colItemDataContainer, { width: 200, alignItems: 'center', borderRightWidth: 3 }]}>
                    <CText style={{ ...stylesWeb.colTitleData }} bold>
                        {_sumValue(data)} 
                    </CText>
                </View>
            </View>
        </View>
    );
};

//make this component available to the app
export default CTableFinalResult;

// define your styles
const styles = StyleSheet.create({
    container: {

    },
    colContainer: {
        flexDirection: 'row',
        backgroundColor: 'brown',
    },
    colItemContainer: {
        padding: 5,
        width: 100,
        alignItems: 'center'
    },
    colItemDataContainer: {
        padding: 5,
        width: 100,
        alignItems: 'center',
        borderLeftWidth: 3,
        borderTopWidth: 3,
        borderBottomColor: 3,
        borderColor: 'brown'
    },
    colorPotensial: (value) => ({
        backgroundColor: 'red'
    })
});

const stylesWeb = {
    colTitleSection: {
        color: 'white',
        
    },
    colTitleData: {
        color: 'black'
    }
}