import React from 'react';
import {View, StyleSheet, CheckBox, TouchableOpacity} from 'react-native-web';
import CText from '../../../components/Text/CText'

const CSelectItem = ({label, active, onSelect}) => {
    return (
        <View style={styles.container}>
            <CheckBox onValueChange={onSelect} value={active} color='royalblue' style={{ width: 20, height: 20, marginRight: 5 }} />
            <TouchableOpacity onPress={onSelect}>
                <CText bold={active}>{label}</CText>
            </TouchableOpacity>
        </View>
    );
}

export default CSelectItem;

const styles = StyleSheet.create({
    container:{
        flexDirection: 'row'
    }
})

