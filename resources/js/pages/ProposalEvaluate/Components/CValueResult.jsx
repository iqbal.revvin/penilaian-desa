//import liraries
import React, { Component, useState, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native-web';
import CText from '../../../components/Text/CText';

// create a component
const CValueResult = ({data}) => {
    const [result, setResult] = useState(0)

    useEffect(() => {
        _sumValue(data)
        return () => {
            false
        };
    }, [data]);


    const _sumValue = (dataCompute) => {
        let sumResult = 0;
        dataCompute && dataCompute.map(item => {
            sumResult += item.aspect
        })
        return setResult(sumResult.toFixed(1))
    }

    const resultCircleColor = () => {
        let color = 'white'
        switch (true) {
            case (result >= 0 && result <= 25):
                color = 'red'
                break;
            case (result >= 26 && result <= 50):
                color = 'orange'
                break;
            case (result >= 51 && result <= 75):
                color = 'yellow'
                break;
            default:
                color = 'chartreuse'
                break;
        }
        return {
            borderColor: color
        }
    }

    return (
        <View style={styles.container}>
            <View style={[styles.circleContainer, resultCircleColor(), {backgroundColor: 'ghostwhite'}]}>
                <CText bold style={{ fontSize:42, color:'black' }}>{result} </CText>
            </View>
                
        </View>
    );
};

//make this component available to the app
export default CValueResult;

// define your styles
const styles = StyleSheet.create({
    container: {

    },
    circleContainer: {
        width: 200,
        height: 200,
        borderRadius: 100,
        alignItems: 'center',
        borderWidth: 30,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

