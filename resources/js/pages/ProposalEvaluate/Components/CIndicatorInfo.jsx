//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native-web';
import CText from '../../../components/Text/CText'

// create a component
const CIndicatorInfo = () => {
    return (
        <View style={styles.container}>
            <ItemIndicator color='chartreuse' rangeDesc='76-100' info='Direkomendasikan' />
            <ItemIndicator color='yellow' rangeDesc='51-75' info='Direkomendasikan' />
            <ItemIndicator color='orange' rangeDesc='26-50' info='Belum dapat direkomendasikan' />
            <ItemIndicator color='red' rangeDesc='0-25' info='Belum dapat direkomendasikan' />
        </View>
    );
};

const ItemIndicator = ({color, rangeDesc, info}) => {
    return(
        <View style={styles.itemIndicatorContainer}>
            <View style={[styles.colorSection, {backgroundColor:color}]} />
            <View style={{ width:65, left:10, alignItems: 'center' }}>
                <CText style={{ fontSize:17 }}>{rangeDesc}</CText>
            </View>
            <View style={{ marginHorizontal:10 }}>
                <CText style={{ fontSize:17 }}>{info}</CText>
            </View>
        </View>
    )
}

// define your styles
const styles = StyleSheet.create({
    container: {
   
    },
    itemIndicatorContainer:{
        flexDirection:'row',
        alignItems: 'center'
    },
    colorSection:{
        width:35,
        height:30,
    }
});

//make this component available to the app
export default CIndicatorInfo;
