import ReactDOM from 'react-dom'
import { Persistor, Store } from '../../services/Redux/store'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import ProposalEvaluate from './ProposalEvaluate'

const rootElement = document.getElementById('proposal-evaluate')
if(rootElement){
    const userLoggedIn = rootElement.getAttribute('userLoggedIn')
    ReactDOM.render(
        <Provider store={Store}>
            <PersistGate loading={null} persistor={Persistor}>
                <ProposalEvaluate userLoggedIn={userLoggedIn} />
            </PersistGate>
        </Provider>,
        rootElement
    )
}