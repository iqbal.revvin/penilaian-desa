import API, { GET_PROPOSAL_EVALUATE, INSERT_ASPECT, INSERT_IMAGE, INSERT_PROVISION, SUBMIT_PROPOSAL, VALIDASI_PENILAI } from "../../../services/api/ApiContainer"

export const getQuestionerProposal = async (userPenilaiId, userDesaId, proposalId, type) => {
    try {
        const request = await API.get(`${GET_PROPOSAL_EVALUATE}/${userPenilaiId}/${userDesaId}/${proposalId}/${type}`)
        return request
    } catch (error) {
        console.log('Proposal.action@getQuestionerProposal', error)
    }
}

export const insertProvision = async (data) => {
    try {
        const request = await API.post(INSERT_PROVISION, data)
        return request
    } catch (error) {
        console.log('Proposal.action@insertProvision', error)
    }
}

export const insertAspect = async (data) => {
    try {
        const request = await API.post(INSERT_ASPECT, data)
        return request
    } catch (error) {
        console.log('Proposal.action@insertAspect', error)
    }
}

export const submitProposal = async (userId, proposalId) => {
    try {
        const request = await API.post(`${SUBMIT_PROPOSAL}/${userId}/${proposalId}`)
        return request
    } catch (error){
        console.log('Proposal.action@submitProposal', error)
    }
}

export const insertImage = async (data) => {
    try {
        const form = new FormData() 
        form.append('user_id', data.user_id)
        form.append('proposal_id', data.proposal_id)
        form.append('name', data.name)
        form.append('aspect_id', data.aspect_id)
        form.append('image', data.image)
        const request = await API.post(INSERT_IMAGE, form)
        return request
     
    } catch (error) {
        console.log('Proposal.action@insertImage', error)
    }
}

export const validatePenilai = async (userPenilaiId, proposalId, data) => {
    try {
        const request = await API.post(`${VALIDASI_PENILAI}/${userPenilaiId}/${proposalId}`, data)
        return request
    } catch (error){
        console.log('ProposalEvaluate.action@validatePenilai', error)
    }
}