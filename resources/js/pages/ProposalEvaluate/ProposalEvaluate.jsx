import React, { Component, Fragment } from 'react'
import { StyleSheet } from 'react-native-web'
import ContentEvaluate from './ContentEvaluate'
import ContentResult from './ContentResult'
import ContentEvaluatePrint from './ContentEvaluatePrint'
class ProposalEvaluate extends Component {
    constructor() {
        super()
        this.state = {
            isLoadingPage: false,
            loadingMessage: 'Mohon Tunggu',
        }
        this.pathUrl = window.location.pathname?.split('/')
        this.contentType = this.pathUrl[6]
    }

    componentDidMount() {
        // if(!this.contentType && this.contentType != 'evaluate' || this.contentType != 'result'){
        //     window.location.replace('/admin')
        // }else{
        //     return false
        // }
        // if(this.contentType != 'evaluate'){
        //     window.location.replace('/admin')
        // }
        // if(this.contentType != 'evaluate'){
        //     window.location.replace('/admin')
        // }
    }


    render() {
        return (
            <Fragment>
                {this.contentType && this.contentType == 'evaluate' && (
                    <ContentEvaluate {...this.props} />
                )}
                {this.contentType && this.contentType == 'evaluate-print' && (
                    <ContentEvaluatePrint {...this.props} />
                )}
                {this.contentType && this.contentType == 'result' && (
                    <ContentResult {...this.props} />
                )}
                {this.contentType != 'evaluate' && this.contentType != 'result' && this.contentType != 'evaluate-print' && (
                    <div>
                        <div className='text-center font-sans font-semibold text-2xl'>
                            MOHON MENGAKSES HALAMAN DENGAN CARA YANG VALID
                        </div>
                        <div className='text-center font-sans font-semibold text-xl mt-2'>
                            <a href='/admin'>KEMBALI KE DASHBOARD</a>
                        </div>
                    </div>
                )}
                {/* {this.contentType == 'proposal-evaluate' && (
                    <ContentEvaluate {...this.props} />
                )} */}
            </Fragment>
        )
    }
}



export default ProposalEvaluate

const styles = StyleSheet.create({
    autoSavingSection: {
        color: 'darkgray',
        position: 'absolute',
        top: -15,
        bottom: 0,
        left: 0,
        right: 0
    },
    cardItem: {
        marginVertical: 5,
        marginTop: 10,
        padding: 15,
        backgroundColor: 'white',
        borderRadius: 5
    },
    formDesaContainer: {
        backgroundColor: 'transparent',
        marginTop: 20
    },
    formSurveyContainer: {
        backgroundColor: 'transparent',
        alignItems: 'center',
        marginTop: 25
    },
    collapContainer: {
        width: '100%',
        marginVertical: 5
    },
    validationContainer: {
        alignItems: 'flex-end',
        marginTop: 10
    }
})
