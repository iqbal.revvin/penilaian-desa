import React, { Component } from 'react'
import { StyleSheet, View, ScrollView } from 'react-native-web'
import CText from '../../components/Text/CText'
import CTextInput from '../../components/TextInput/CTextInput'
import CCollapsible from '../Proposal/Components/CCollapsible'
import { connect } from 'react-redux'
import {
    CHANGE_VIDEO_URL, ENTRY_DESCRIPTION, INSERT_PROVISION_DATA, SELECT_CRITERIA, SELECT_PROVISION, SET_VALIDATE_ALLOW,
    SET_VALIDATE_DENY, UPDATE_ASPECT_DATA, UPDATE_MASTER_DATA_CRITERIA, UPDATE_PROVISION_DATA, UPDATE_SUBMISSION_PROFILE
} from '../../services/Redux/reducers/ProposalReducer'
import CSelectItem from '../Proposal/Components/CSelectItem'
import CUploadImage from '../../components/CUploadImage'
import CButtonRegular from '../../components/Buttons/CButtonRegular'
import CLoading from '../../components/Loaders/CLoading'
import { iconWarning, iconDocValidate } from '../../assets/images/icons'
import { getQuestionerProposal } from '../Proposal/services/actions'
import CFieldInfo from './Components/CFieldInfo'
import { printIcon } from '../../assets/images/icons'
import CNumberInput from '../../components/TextInput/CNumberInput'

class ProposalReview extends Component {
    constructor() {
        super()
        this.state = {
            isLoadingPage: false,
            loadingMessage: 'Mohon Tunggu',
            count: 0,
            active: false,
            autosave: false,
            isShowPopup: false,
            popupConfig: false
        }
        this.pathUrl = window.location.pathname?.split('/')
        this.userId = this.pathUrl[3]
        this.proposalId = this.pathUrl[4]
    }

    componentDidMount() {
        this._handleCheckInQuestioner()
    }

    componentDidUpdate() {

    }

    _handleCheckInQuestioner = () => {
        // console.log(JSON.stringify(pathUrl))
        if (isNaN(this.userId) || isNaN(this.proposalId)) {
            this.setState({
                isLoadingPage: true,
                isShowPopup: true,
                popupConfig: {
                    icon: iconWarning,
                    title: 'Akses Tidak Valid',
                    message: 'Pastikan anda mengakses data pengisian nilai poposal dengan cara yang valid!',
                    colorButtonPrimary: `teal`,
                    buttonPrimaryText: 'TUTUP',
                    buttonPrimaryAction: () => window.location.replace('/admin'),
                    buttonSecondaryAction: () => window.location.replace('/admin'),
                }
            })
        } else {
            if (this.props.userId !== this.userId) {
                this.setState({
                    isLoadingPage: true,
                    isShowPopup: true,
                    popupConfig: {
                        icon: iconWarning,
                        title: 'Akses Tidak Valid',
                        message: 'Pastikan anda mengakses data pengisian nilai poposal dengan cara yang valid!',
                        colorButtonPrimary: `teal`,
                        buttonPrimaryText: 'TUTUP',
                        buttonPrimaryAction: () => window.location.replace('/admin'),
                        buttonSecondaryAction: () => window.location.replace('/admin'),
                    }
                })
            } else {
                this._handleFetchQuestionerProposal(this.userId, this.proposalId, 'review')
            }
        }
    }

    _handleFetchQuestionerProposal = async (userId, proposalId, type) => {
        this.setState({ isLoadingPage: true, loadingMessage: 'Memuat Data Questioner Proposal' })
        try {
            const response = await getQuestionerProposal(userId, proposalId, type)
            if (response.data?.success) {
                this.setState({ isLoadingPage: false })
                this.props.UpdataMasterDataCriteria(response.data.output_data.questioner)
                this.props.UpdateSubmissionProfile(response.data.output_data.submission_profile)

            } else {
                let icon = ''
                let title = ''
                let message = ''
                if (response.data.message != 'proposal-submited') {
                    icon = iconWarning
                    title = 'Terjadi Masalah'
                    message = 'Mohon maaf, terjadi masalah saat mengakses data questioner proposal'
                } else {
                    icon = iconDocValidate
                    title = 'Proposal Tervalidasi'
                    message = 'Mohon maaf, proposal yang telah divalidasi tidak dapat di ubah kembali'
                }
                this.setState({
                    isLoadingPage: true,
                    isShowPopup: true,
                    popupConfig: {
                        icon: icon,
                        title: title,
                        message: message,
                        colorButtonPrimary: `teal`,
                        buttonPrimaryText: 'TUTUP',
                        buttonPrimaryAction: () => window.location.replace('/admin/proposal'),
                        buttonSecondaryAction: () => window.location.replace('/admin/proposal'),
                    }
                })
            }
        } catch (error) {
            this.setState({
                isLoadingPage: true,
                isShowPopup: true,
                popupConfig: {
                    icon: iconWarning,
                    title: 'Terjadi Masalah',
                    message: 'Mohon maaf, terjadi masalah saat mengakses data questioner proposal',
                    colorButtonPrimary: `teal`,
                    buttonPrimaryText: 'TUTUP',
                    buttonPrimaryAction: () => window.location.replace('/admin'),
                    buttonSecondaryAction: () => window.location.replace('/admin'),
                }
            })
        }
    }

    render() {
        const { proposalState: { criteria, submission_profile } } = this.props
        const { isLoadingPage, loadingMessage } = this.state
        return (
            <div className='mx-auto my-4'>   
                {isLoadingPage && <CLoading message={loadingMessage} />}
                <div className='antialiased flex flex-row justify-center'>
                    <div className='w-full lg:w-9/12'>
                        <CButtonRegular label='Cetak Halaman' color='ghostwhite' icon={printIcon} 
                            colorText={'black'} onPress={() => window.print()} />
                        <ScrollView>
                            <View style={styles.formDesaContainer}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ marginHorizontal: 10 }}>
                                        <CFieldInfo label='Nama Desa' value={submission_profile.nama_desa} />
                                        <CFieldInfo label='Nama Desa Wisata' value={submission_profile.nama_desa_wisata} />
                                        <CFieldInfo label='Nama Kepala Desa' value={submission_profile.nama_kepala_desa} />
                                        <CFieldInfo label='Email' value={submission_profile.email} />
                                        <CFieldInfo label='No. HP/WA' value={submission_profile.no_hp} />
                                    </View>
                                    <View style={{ marginHorizontal: 10 }}>
                                        <CFieldInfo label='Provinsi' value={submission_profile.provinsi} />
                                        <CFieldInfo label='Kabupaten' value={submission_profile.kabupaten} />
                                        <CFieldInfo label='Kecamatan' value={submission_profile.kecamatan} />
                                        <CFieldInfo label='Kelurahan' value={submission_profile.nama_desa} />
                                        <CFieldInfo label='Jumlah Pengunjung' value={new Intl.NumberFormat().format(submission_profile.jumlah_pengunjung)} />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.formSurveyContainer}>
                                {criteria.map((criteria, i) => {
                                    return (
                                        <View key={i} style={styles.collapContainer}>
                                            <CCollapsible label={criteria.name} active={true}>
                                                {criteria.aspect.map((aspect, aspectIdx) => {
                                                    const urlVideo = aspect.video_url_1
                                                    const urlVideo2 = aspect.video_url_2
                                                    return (
                                                        <View key={aspectIdx} style={styles.cardItem}>
                                                            <CText bold>
                                                                Jabarkan secara singkat dari {aspect.name} yang dimiliki
                                                                Desa Wisata Yang Diajukan!
                                                            </CText>
                                                            <CText>Dilengkapi dengan dokumen pendukung berupa foto dan atau video</CText>
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <CText bold>Isian diharapkan misal : </CText>
                                                                <CText>Jenis/Bentuk {aspect.name} yang dimiliki dan lainnya</CText>
                                                            </View>
                                                            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                                                <View style={{ marginVertical: 5 }}>
                                                                    {aspect.provision.map((provision, provisionIdx) => {
                                                                        return (
                                                                            <View key={provisionIdx} style={{ marginVertical: 5 }}>
                                                                                <CSelectItem label={provision.name} active={provision.selected} />
                                                                            </View>
                                                                        )
                                                                    })}
                                                                </View>
                                                            </ScrollView>
                                                            <View style={{ margin: 2, height: 100 }}>
                                                                <CTextInput value={aspect.description_entry} multiline numberOfLines={2} placeholder='Deskripsi Singkat' />
                                                            </View>
                                                            <View style={{ margin: 2, marginTop: 5, flexDirection: 'row', alignItems: 'center' }}>
                                                                <CText style={{ marginRight: 10 }}>URL Video Youtube 1: </CText>
                                                                <View style={{ width: '55%' }}>
                                                                    <CTextInput value={aspect.video_url_1} placeholder='Link Video Youtube' />
                                                                </View>
                                                            </View>
                                                            <View style={{ margin: 2, marginTop: 5, flexDirection: 'row', alignItems: 'center' }}>
                                                                <CText style={{ marginRight: 10 }}>URL Video Youtube 2: </CText>
                                                                <View style={{ width: '55%' }}>
                                                                    <CTextInput value={aspect.video_url_2} placeholder='Link Video Youtube 2' />
                                                                </View>
                                                            </View>
                                                            <View style={{ marginTop: 10, backgroundColor: 'transparent', width: '70%', alignSelf: 'center' }}>
                                                                <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                                                                    {aspect.images.map((images, imagesIdx) => {
                                                                        return (
                                                                            <View key={imagesIdx} style={{ marginRight: 10, marginBottom: 10 }}>
                                                                                {/* <input type="file" name="myImage" onChange={this._handleChangeImage(images.name, aspect.id)}/> */}
                                                                                <CUploadImage initSrc={images.url} disabled />
                                                                            </View>
                                                                        )
                                                                    })}
                                                                </View>
                                                            </View>
                                                            <br /><hr /><hr />
                                                        </View>
                                                    )
                                                })}
                                            </CCollapsible>
                                        </View>
                                    )
                                })}
                            </View>
                        </ScrollView>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    proposalState: state.proposal
})

const mapDispatchToProps = (dispatch) => ({
    UpdateSubmissionProfile: (data) => dispatch({ type: UPDATE_SUBMISSION_PROFILE, payload: data }),
    UpdataMasterDataCriteria: (data) => dispatch({ type: UPDATE_MASTER_DATA_CRITERIA, payload: data }),
})

export default connect(mapStateToProps, mapDispatchToProps)(ProposalReview)

const styles = StyleSheet.create({
    autoSavingSection: {
        color: 'darkgray',
        position: 'absolute',
        top: -15,
        bottom: 0,
        left: 0,
        right: 0
    },
    cardItem: {
        marginVertical: 5,
        marginTop: 10,
        padding: 15,
        backgroundColor: 'white',
        borderRadius: 5
    },
    formDesaContainer: {
        backgroundColor: 'transparent',
        marginTop: 20
    },
    formSurveyContainer: {
        backgroundColor: 'transparent',
        alignItems: 'center',
        marginTop: 25
    },
    collapContainer: {
        width: '100%',
        marginVertical: 5
    },
    validationContainer: {
        alignItems: 'flex-end',
        marginTop: 10
    }
})
