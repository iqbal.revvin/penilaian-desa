//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native-web';
import CText from '../../../components/Text/CText'

// create a component
const CFieldInfo = ({label, value}) => {
    return (
        <View style={styles.container}>
            <View style={styles.fieldInfoContainer}>
                <View style={{ backgroundColor:'transparent', width: 225 }}>
                    <CText style={stylesWeb.textStyle}>{label}</CText>
                </View>
                <View style={{ backgroundColor:'transparent', alignItems: 'center', width:25 }}>
                    <CText style={stylesWeb.textStyle}>:</CText>
                </View>
                <View style={{ backgroundColor:'transparent', alignItems: 'flex-start', minWidth:200 }}>
                    <CText style={stylesWeb.textStyle} bold>{value}</CText>
                </View>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 10
    },
    fieldInfoContainer:{
        flexDirection: 'row'
    },
});

const stylesWeb = {
    textStyle:{
        color: 'black',
        fontSize: 18
    }
}

//make this component available to the app
export default CFieldInfo;
