import ReactDOM from 'react-dom'
import { Persistor, Store } from '../../services/Redux/store'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import ProposalReview from './ProposalReview'

const rootElement = document.getElementById('proposal-review')
if(rootElement){
    const userId = rootElement.getAttribute('userId')
    ReactDOM.render(
        <Provider store={Store}>
            <PersistGate loading={null} persistor={Persistor}>
                <ProposalReview userId={userId} />
            </PersistGate>
        </Provider>,
        rootElement
    )
}