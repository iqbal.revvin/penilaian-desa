export const iconWarning = require('./warning.svg').default;
export const iconSuccess = require('./success.png').default;
export const iconDown = require('./icon-down.png').default;
export const iconCheck = require('./icon-check.png').default;
export const iconDocValidate = require('./doc-validate.png').default;
export const printIcon = require('./print-icon.png').default;