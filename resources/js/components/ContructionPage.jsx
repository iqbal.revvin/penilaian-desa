import React from 'react'

const ConstructionPage = ({info}) => {
    return (
        <section className="header items-center flex h-screen max-h-860-px">
            <div className="container mx-auto items-center flex flex-wrap">
                <div className="w-full md:w-8/12 lg:w-6/12 xl:w-6/12 px-4">
                    <div className="pt-32 sm:pt-0">
                        <h2 className="font-semibold text-4xl text-blueGray-600">
                           Fitur sedang dalam pengembangan
                        </h2>
                        <p className="mt-4 text-lg leading-relaxed text-blueGray-500">
                            {info}
                        </p>
                        <div className="mt-12">
                            <a href="/"
                                className="text-white font-bold px-6 py-4 rounded outline-none focus:outline-none mr-1 mb-1 bg-sky-600 active:bg-sky-700 uppercase text-sm shadow hover:shadow-lg ease-linear transition-all duration-150">
                                Kemabali Ke Halaman Utama
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <img className="absolute top-0 b-auto right-0 sm:w-6/12 -mt-48 sm:mt-0 w-10/12 max-h-860px"
                src={require('../assets/images/bg/pattern_react.png').default} alt="VCDLN Learning Construction"
            />
        </section>
    )
}

ConstructionPage.defaultProps = {
    info: 'Fitur materi adalah daftar materi dari berbagai jenjang dan mata pelajaran yang telah di validasi oleh tim kami yang dapat di akses oleh peserta didik maupun umum.'
}

export default ConstructionPage