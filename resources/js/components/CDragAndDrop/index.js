import React, { Component } from 'react'
import PropTypes from 'prop-types';

/**
 * CDragAndDrop
 * @augments{Component<Props,State>}
 */

class CDragAndDrop extends Component {
    static propTypes = {
        handleDrag: PropTypes.func,
        handleDragIn: PropTypes.func,
        handleDragOut: PropTypes.func,
        handleDrop: PropTypes.func,
        onPress: PropTypes.func,
    }

    dropRef = React.createRef()

    handleDrag = (e) => {
        e.preventDefault()
        e.stopPropagation()

        !!this.props.handleDrag && this.props.handleDrag(e)
    }

    handleDragIn = (e) => {
        e.preventDefault()
        e.stopPropagation()

        !!this.props.handleDragIn && this.props.handleDragIn(e)
    }

    handleDragOut = (e) => {
        e.preventDefault()
        e.stopPropagation()

        !!this.props.handleDragOut && this.props.handleDragOut(e)
    }

    handleDrop = (e) => {
        e.preventDefault()
        e.stopPropagation()


        if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {

            !!this.props.handleDrop && this.props.handleDrop(e.dataTransfer.files[0])

            console.log("file selected", e.dataTransfer.files[0])
            e.dataTransfer.clearData()
        }
    }

    componentDidMount() {
        let div = this.dropRef.current
        div.addEventListener('dragenter', this.handleDragIn)
        div.addEventListener('dragleave', this.handleDragOut)
        div.addEventListener('dragover', this.handleDrag)
        div.addEventListener('drop', this.handleDrop)
    }

    componentWillUnmount() {
        let div = this.dropRef.current
        div.removeEventListener('dragenter', this.handleDragIn)
        div.removeEventListener('dragleave', this.handleDragOut)
        div.removeEventListener('dragover', this.handleDrag)
        div.removeEventListener('drop', this.handleDrop)
    }

    render() {
        return (
            <div ref={this.dropRef}>
                {this.props.children}
            </div>
        )
    }
}
export default CDragAndDrop