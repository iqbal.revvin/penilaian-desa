import React, { Component, PureComponent } from 'react'
import { Image, TouchableOpacity, View } from 'react-native-web'
import CText from '../Text/CText'
import PropTypes from 'prop-types';
/**
 * CUploadImage
 * @augments{Component<Props,State>}
 */

class CUploadImage extends PureComponent {
    static propTypes = {
        onChange: PropTypes.func, // (file) => {}
        identifier : PropTypes.string, // must unique
        initSrc : PropTypes.string,
        maxLimit: PropTypes.number, // maxLimit={500} it means maximal image to upload 500kb; default is 20000 (2mb)
        unitSizeErrorMsg: PropTypes.string, // default (mb)
        disabled: PropTypes.bool
    }

    constructor(props) {
        super(props)

        this.generateIdImagePreview = props.identifier ? props.identifier : 'imagePreview-' + new Date().valueOf()
        this.state = {
            file: false,
            errorMessageFile: '',
            initSrc: props.initSrc
        }
    }

    dropRef = React.createRef()
    inputRef = React.createRef()

    handleDrag = (e) => {
        e.preventDefault()
        e.stopPropagation()

        !!this.props.handleDrag && this.props.handleDrag(e)
    }

    handleDragIn = (e) => {
        e.preventDefault()
        e.stopPropagation()

        !!this.props.handleDragIn && this.props.handleDragIn(e)
    }

    handleDragOut = (e) => {
        e.preventDefault()
        e.stopPropagation()

        !!this.props.handleDragOut && this.props.handleDragOut(e)
    }

    removeUploadPreview = () => {
        this.setState({
            file: false,
            initSrc: ''
        }, this.handleOnChange)
        document.getElementById(this.generateIdImagePreview)
            .src = '#'

        this.inputRef.current.value = ''
    }

    handleOnChange = () => {
        const { file } = this.state
        console.log('--onChange Props @CUploadImage', file)

        !!this.props.onChange && this.props.onChange(file)
    }

    handleDrop = (e) => {
        e.preventDefault()
        e.stopPropagation()


        if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
            const file = e.dataTransfer.files[0]
            const isValidType = this._handleValidationExtension(file?.type)
            const isValidSize = this._handleValidationMaximumSize(file?.size)

            // console.log("file drop", file)
            if (isValidSize && isValidType) {
                const preview = document.getElementById(this.generateIdImagePreview)
                preview.src = URL.createObjectURL(file)
                preview.style.visibility = 'visible'
                this.setState({
                    file,
                    errorMessageFile: ''
                }, this.handleOnChange)
            }
            e.dataTransfer.clearData()
        }
    }

    handleInputFileChange = (event) => {
        event.stopPropagation();
        event.preventDefault();

        const file = event?.target?.files[0]
        const isValidType = this._handleValidationExtension(file?.type)
        const isValidSize = this._handleValidationMaximumSize(file?.size)
        // console.log('file input', file)
        if (isValidSize && isValidType) {
            const preview = document.getElementById(this.generateIdImagePreview)
            preview.src = URL.createObjectURL(file)
            preview.style.visibility = 'visible'
            this.setState({
                file,
                errorMessageFile: ''
            }, this.handleOnChange)
        }
    }

    _handleValidationExtension = (type) => {
        let _isValid = false
        if (type === 'image/jpeg' || type === 'image/png') {
            _isValid = true
        } else {
            _isValid = false
            this.setState({
                errorMessageFile: 'Only JPEG or PNG file are allowed'
            })
        }

        return _isValid
    }

    _handleValidationMaximumSize = (size) => {
        console.log('file Sized(kb) ==>', size/1000)
        let _isValid = true
        const maxLimit = this.props.maxSize ? this.props.maxSize * 1000 : 2000000 //2 Mb
        const unitSize = this.props.unitSizeErrorMsg ? this.props.unitSizeErrorMsg : 'mb'
        const errorMsg = unitSize === 'mb' ? `Max file size: ${maxLimit / 1000000}mb` : `Max file size: ${maxLimit / 1000}kb`

        if (size < maxLimit) {
            _isValid = true
        } else {
            _isValid = false
            this.setState({
                errorMessageFile: errorMsg
            })
        }

        return _isValid
    }

    handleMouseOver = () => {
        let input = this.inputRef.current
        input.style.cursor = 'pointer'
    }

    hideBrokenImage = () => {
        document.getElementById(this.generateIdImagePreview)
            .style.visibility = 'hidden'
    }

    componentDidMount() {
        let div = this.dropRef.current
        let input = this.inputRef.current
        div.addEventListener('dragenter', this.handleDragIn)
        div.addEventListener('dragleave', this.handleDragOut)
        div.addEventListener('dragover', this.handleDrag)
        div.addEventListener('drop', this.handleDrop)
        input.addEventListener("mouseover", this.handleMouseOver, false);
    }

    componentWillUnmount() {
        let div = this.dropRef.current
        let input = this.inputRef.current
        div.removeEventListener('dragenter', this.handleDragIn)
        div.removeEventListener('dragleave', this.handleDragOut)
        div.removeEventListener('dragover', this.handleDrag)
        div.removeEventListener('drop', this.handleDrop)
        input.removeEventListener("mouseover", this.handleMouseOver, false);
    }

    render() {
        const { file, initSrc } = this.state
        const {disabled} = this.props
        // console.log('idgenerate', this.generateIdImagePreview)
        return (
            <View>
                <div style={styles.outerContainer} ref={this.dropRef} >
                    {
                        !file && !initSrc &&
                        <View style={styles.containerEmptyFile}>
                            <Image source={require('./icon-upload.png').default} style={{ height: 50, width: 50, resizeMode: 'contain' }} />
                            <CText bold style={{ color: '#999999', fontSize: 12, textAlign: 'center', marginTop: 16 }}>
                                {`Click to browse flile \nor \nDrag your file here`}
                            </CText>
                            <CText bold style={{ color: '#FF0025', fontSize: 10, textAlign: 'center', marginTop: 8 }}>
                                {this.state.errorMessageFile}
                            </CText>
                        </View>
                    }
                    {initSrc && (
                        <img src={initSrc} style={styles.imagePreview} id={this.generateIdImagePreview} onError={this.hideBrokenImage} />
                    )}
                    {!initSrc && (
                        <img style={styles.imagePreview} id={this.generateIdImagePreview} onError={this.hideBrokenImage} />
                    )}
                    <input type={!disabled?"file":null} style={styles.inputForm} ref={this.inputRef} onChange={!disabled?this.handleInputFileChange:() => alert('Upload disabled')} />
                    {
                        (file || initSrc && !disabled) &&
                        <TouchableOpacity style={styles.iconClosePosition} onPress={this.removeUploadPreview}>
                            <View style={{ padding: 4 }}>
                                <Image source={require('./icon-close.svg').default} style={{ height: 25, width: 25 }} />
                            </View>
                        </TouchableOpacity>
                    }
                </div>
            </View>
        )
    }
}

const styles = {
    outerContainer: {
        height: 140,
        width: 200,
        borderWidth: 3,
        borderStyle: 'dashed',
        borderColor: 'darkgray',
        borderRadius: 10,
        position: 'relative'
    },
    containerEmptyFile: {
        position: 'absolute',
        zIndex: -1,
        height: 135,
        width: 200,
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputForm: {
        backgroundColor: 'blue',
        height: '100%',
        width: '100%',
        opacity: 0
    },
    imagePreview: {
        height: 140,
        width: 200,
        objectFit: 'contain',
        position: 'absolute',
    },
    iconClosePosition: {
        position: 'absolute',
        top: 4,
        end: 4
    }
}

export default CUploadImage