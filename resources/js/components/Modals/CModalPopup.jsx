import React, { Fragment, useEffect } from "react";
import { Animated, View } from "react-native-web";
import { iconWarning } from '../../assets/images/icons'

const CModalPopup = ({ popupData }) => {
    // const [showModal, setShowModal] = React.useState(true);
    const AnimatedPopup = new Animated.Value(0)

    console.log('popup')
    useEffect(() => {
        Animated.spring(AnimatedPopup, {
            toValue: 1,
            friction: 5,
            tension: 75
        }).start()
       
    }, []);


    const styles = {
        buttonCancel: `text-${popupData.colorButtonSecondary}-600 bg-pink-600 text-white font-bold uppercase px-6 py-3 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 rounded`,
        buttonNext: `w-full bg-${popupData.colorButtonPrimary}-600 text-white active:bg-${popupData.colorButtonPrimary}-700 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-200`,
        buttonNext_test: `w-full bg-teal-600 text-white`,
        buttonNext_test2: `w-full bg-yellow-600 text-white`
    }
    const RenderModal = () => (
        <Fragment>
            <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                <Animated.View style={{ transform: [{ scale: AnimatedPopup }] }}>
                    <div className="relative w-auto my-6 mx-auto max-w-sm">
                        {/*content*/}
                        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none min-w-[350px]">
                            {/*header*/}
                            <div className="flex items-start justify-between px-2 rounded-t">
                                <button className="ml-auto border-0 text-black font-bold hover:bg-transparent" onClick={popupData.buttonSecondaryAction}>
                                    <span className="text-black opacity-25 w-6 text-2xl block focus:outline-none">
                                        ×
                                    </span>
                                </button>
                            </div>
                            {/*body*/}
                            <div className="relative p-6 flex flex-col items-center">
                                <div className='mb-5'>
                                    <img src={popupData.icon} style={{ width: 150, height: 150 }} />
                                </div>
                                <h2 className='font-bold text-xl text-blueGray-600 text-center'>{popupData.title}</h2>
                                <p className="my-4 text-blueGray-500 text-lg leading-relaxed text-center">
                                    {popupData.message}
                                </p>
                            </div>
                            {/*footer*/}
                            <div className="flex items-center justify-between p-6 border-t border-solid border-blueGray-200 rounded-b">
                                {popupData.buttonSecondaryText && (
                                    <button type="button" className={styles.buttonCancel} onClick={popupData.buttonSecondaryAction} >
                                        {popupData.buttonSecondaryText}
                                    </button>
                                )}
                                <button type="button" className={styles.buttonNext} onClick={popupData.buttonPrimaryAction}>
                                    {popupData.buttonPrimaryText}
                                </button>
                            </div>
                        </div>
                    </div>
                </Animated.View>
            </div>

            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </Fragment>
    )

    return (
        <Fragment>
            <RenderModal />
        </Fragment>
    );

}

CModalPopup.defaultProps = {
    popupData: {
        icon: iconWarning,
        title: 'Title Popup',
        message: 'Message Popup',
        colorButtonPrimary: 'sky',
        colorButtonSecondary: 'red',
        buttonPrimaryText: 'Primary Button',
        buttonSecondaryText: 'Secondary Button',
        buttonPrimaryAction: () => alert('ButtonPrimaryAction'),
        buttonSecondaryAction: () => alert('ButtonSecondaryAction')
    }
}

const compare = (prevProps, nextProps) => {
    return JSON.stringify(prevProps) === JSON.stringify(nextProps)
}

export default React.memo(CModalPopup, compare)