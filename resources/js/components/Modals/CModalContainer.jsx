import React, {Fragment} from 'react'

const CModalContainer = ({children, onClose}) => {
    return (
        <Fragment>
            <div className={tailStyle.container} onClick={(onClose)}>
                <div className="relative w-auto my-6 mx-auto max-w-6xl">
                    {/*content*/}
                    <div className={tailStyle.contentContainer}>
                        {children}
                    </div>
                </div>
            </div>
            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </Fragment>
    )
}

const compare = (prevProps, nextState) => {
    return JSON.stringify(prevProps, nextState)
}

export default React.memo(CModalContainer, compare)

const tailStyle = {
    container: 'justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none',
    contentContainer:'border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none'
}