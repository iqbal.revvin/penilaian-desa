//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native-web';

// create a component
const CNumberInput = ({value, onChange}) => {
    return (
        <View style={styles.container}>
            <input type='number' value={value} onChange={onChange} className='w-auto outline-none rounded-sm' min='0' max='100' />
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        
    },
});

//make this component available to the app
export default CNumberInput;
