import React, {useEffect, useState} from 'react'
import {StyleSheet, View, Image, TouchableOpacity} from 'react-native-web'
import {iconArrowUpOutline, iconArrowDownOutline} from '../../Assets/web/shared'

const CNumberField = ({value, onChange, width}) => {
    const [number, setNumber] = useState(value)

    const handleMin = () => {
        if(number <= 0){
            return;
        }

        setNumber(number-1)
    }

    useEffect(() => {
        onChange(number)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[number])

    useEffect(()=>{
        setNumber(value)
    },[value])

    const _handleOnChange = (event) => {
        // eslint-disable-next-line radix
        const num = parseInt(event.target.value)
        setNumber(num)
    }   

    return(
        <View style={{ width:width }}>
            <View style={styles.fieldContainer}>
                <View style={{ flex:1 }}>
                    <input type='number' value={number} onChange={_handleOnChange} style={{ outline:'none', borderWidth:0, fontFamily:'Muli-Regular' }} />
                </View>
                
                <View>
                    <TouchableOpacity onPress={() => setNumber(number+1)}>
                        <Image source={iconArrowUpOutline} style={styles.iconSection} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={handleMin}>
                        <Image source={iconArrowDownOutline} style={styles.iconSection} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default React.memo(CNumberField)

const styles = StyleSheet.create({
    fieldContainer:{
        flexDirection:'row',
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor:'lightgray',
        borderRadius:5,
        height:50,
        alignItems: 'center',
        paddingHorizontal:10
    },
    iconSection:{
        width:15,
        height:15
    }
})