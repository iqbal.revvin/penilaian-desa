import React from 'react';

const CLoading = ({message}) => {
    return (
        <div className="fixed top-0 left-0 right-0 bottom-0 w-full h-screen z-50 overflow-hidden bg-gray-700 opacity-75 flex flex-col items-center justify-center">
            <div className="loader ease-linear rounded-full border-8 border-t-8 border-gray-500 h-16 w-16 mb-4"></div>
            <h2 className="text-center text-white text-xl font-semibold animate-bounce">{message}</h2>
        </div>
    );
}

CLoading.defaultProps = {
    message: 'Mohon Tunggu'
}

export default CLoading
