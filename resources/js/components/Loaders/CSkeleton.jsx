import React from 'react'
import { View } from 'react-native-web'
import tailwind from 'tailwind-rn'

const CSkeleton = ({style, height, width, color, radius}) => {
    return(
        <View style={{...style, width:width, height:height, borderRadius:radius, overflow: 'hidden' }}>
            <div className='animate-pulse w-full'>
                <div style={{ backgroundColor:color, height: height, width:'100%' }} />
            </div>
        </View>
    )
}

CSkeleton.defaultProps = {
    width:200,
    height: 50,
    radius: 20,
    color:'darkgray'
}

export default CSkeleton
