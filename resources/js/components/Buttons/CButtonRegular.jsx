import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native-web';
import CText from '../Text/CText';
import {iconCheck} from '../../assets/images/icons/index'

const CButtonRegular = ({label, color, onPress, disabled, icon, colorText}) => {
    const styles = styling(color, disabled)
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPress}>
                <View style={styles.buttonContainer}>
                    <View style={{ marginRight:10 }}>
                        <Image source={icon} style={{ height:20, width:20, tintColor:disabled?'lightgrey':'' }} />
                    </View>
                    <View>
                        <CText bold style={{ color:disabled?'lightgray':colorText?colorText:'white', fontSize:16 }}>{label}</CText>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );
}

CButtonRegular.defaultProps = {
    label: 'Button',
    color: '#17A2B8',
    colorText:'white',
    icon: iconCheck,
    onPress: () => alert('On Press'),
    disabled: false
}

export default CButtonRegular;


const styling = (color, disabled) => StyleSheet.create({
    container:{

    },
    buttonContainer:{
        backgroundColor:disabled?'gray':color,
        paddingHorizontal:25,
        minWidth:175,
        height:45,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 3,
        boxShadow: `${5}px ${5}px ${5}px rgba(95,101,255,0.17)`
    }
})

