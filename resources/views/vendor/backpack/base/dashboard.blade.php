@extends(backpack_view('blank'))
@php
    $now = \Carbon\Carbon::now();

    $tahun = \App\Models\TahunAnggaran::where('status', 'Aktif')->first();
    $breadcrumbs = [
        'Dewa Membumi' => backpack_url('dashboard'),
        'Selamat Datang' => false
    ];
@endphp
@php
    $widgets['before_content'][] = [
        'type'        => 'jumbotron',
        'heading'     => '<b>Dewa Membumi</b>',
        'content'     => '
            <h5>Selamat datang di Sistem Informasi Penilaian Pengembangan Desa Wisata<br>
            Melalui Pemetaan Potensi dan Kolaborasi untuk Percepatan Pemulihan<br>
            Ekonomi (DEWA MEMBUMI) di Kabupaten Garut<br><br>Bappeda Kabupaten Garut<br><br>
            Harap memperhatikan info sebagai berikut:<br>
            Pengajuan Proposal Tahun Anggaran 2021 disesuaikan dengan schedule time yaitu:<br><br>
            <ol>• Pengajuan: '.date('d F Y', strtotime($tahun->tanggal_awal_pengajuan)).' s.d '.date('d F Y', strtotime($tahun->tanggal_akhir_pengajuan)).'</ol>
            <hr />
            </h5>
        ',
        'button_link' => backpack_url('logout'),
        'button_text' => 'Keluar Akun',
    ];
@endphp
@section('content')

@endsection