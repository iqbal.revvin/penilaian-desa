<!DOCTYPE html>
{{-- <html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> --}}

<html lang="id">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <meta name="theme-color" content="#000000">
        {{-- <meta name="robots" content="index,follow"> --}}
        <meta name="robots" content="noindex, nofollow" />
        <meta name="title" content="Dewa Membumi">
        <meta name="author" content="dewa membumi">
        <meta name="google-site-verification" content="EYPD272qLP5lw46eALdEsVKxP0uogcbxsdWqkTssniU">
        <link rel="apple-touch-icon" href="{{asset('pavicon.ico')}}">
        <link rel="icon" href="{{asset('pavicon.ico')}}">
        @if($title??'')
            <title>{{env('APP_NAME')}} || {{$title??''}}</title>
        @else
            <title>{{env('APP_NAME')}}</title>
        @endif
 
        <!-- Fonts -->
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/loader.css') }}" rel="stylesheet">
        <link href="{{ asset('webfonts/all.min.css')}}" rel="stylesheet">
        <script src="{{ mix('js/app.js') }}" defer></script>
        <script src="{{ mix('js/module/app.js') }}" defer></script>
    </head>

    <body>
        @yield('content')
    </body>

</html>