@extends('master')
@section('title')
It's not you, it's me.
@endsection
@section('content')
<section class="header items-center flex h-screen max-h-860-px">
	<div class="container mx-auto items-center flex flex-wrap">
		<div class="w-full md:w-8/12 lg:w-6/12 xl:w-6/12 px-4">
			<div class="pt-32 sm:pt-0">
				<h2 class="font-semibold text-4xl text-blueGray-600">
					Sistem sedang dalam perawatan
				</h2>
				<p class="mt-4 text-lg leading-relaxed text-blueGray-500">
					Hallo, mohon maaf saat ini aplikasi sedang dalam peningkatan kualitas sistem, silahkan kembali lagi
					beberapa saat kemudian.
				</p>
				<div class="flex flex-col lg:flex-row">
					<div class="mt-12">
						<a href="http://dewamembumi.bappeda.garutkab.go.id/" target="_blank"
							class="text-white font-bold px-6 py-4 rounded outline-none focus:outline-none mr-1 mb-1 bg-sky-600 active:bg-sky-700 uppercase text-sm shadow hover:shadow-lg ease-linear transition-all duration-150">
							Situs Resmi Dewa Membumi
						</a>
					</div>
					<div class="mt-12">
						<a href="https://bappeda.garutkab.go.id/" target="_blank"
							class="text-white font-bold px-6 py-4 rounded outline-none focus:outline-none mr-1 mb-1 bg-sky-600 active:bg-sky-700 uppercase text-sm shadow hover:shadow-lg ease-linear transition-all duration-150">
							Situs Resmi Bappeda Garut
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<img class="absolute top-0 b-auto right-0 sm:w-6/12 -mt-48 sm:mt-0 w-10/12 max-h-860px" src='{{ asset('
		bg-maintenance.png') }}' alt="Dewa Menbumi Maintenance Page" />
</section>
@endsection